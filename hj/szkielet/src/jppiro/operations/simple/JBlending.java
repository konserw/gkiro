/*
 *  JBlending.java
 *
 *  Zawiera klas� JBlending,
 *  wykonuj�c� ��czenie dw�ch obraz�w.
 */

// definicja pakietu
package jppiro.operations.simple;

// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;

/**
 *  Publiczna klasa JBlending wykonuje ��czenie dw�ch obraz�w.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JBlending extends JOperation
{
    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JBlending() {
        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Blending " );
        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class, JBitmap.class, Double.class );
        setParamDesc( "Obraz podstawowy", "Drugi obraz", "Parametr X" );
    }

    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {
        // wywo�anie funkcji blending()
        return (Object) blending( (JBitmap)param[0],
                                  (JBitmap)param[1],
                                  (Double)param[2] );
    }

    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {
        // obraz podstawowy przekazany jako pierwszy parametr
        JBitmap bin1 =(JBitmap) param[0];

        // obraz dodatkowy przekazany jako drugi parametr
        JBitmap bin2 =(JBitmap) param[1];

        // je�eli bitmapy r�ni� sie rozmiarami
        if( bin1.getWidth() != bin2.getWidth()  || bin1.getHeight() != bin2.getHeight() ) {
            JOptionPane.showMessageDialog( null,
                    "Obrazy maj� r�ne rozmiary",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // parametr 'X' przekazany jako trzeci parametr
        double x =(Double) param[2];

        // je�eli parametr 'X' jest < 0.0
        if( x < 0.0 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� parametru 'X' musi by� >= 0.0",
                    "Niepoprawny parametr!",
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // je�eli parametr 'X' jest > 1.0
        if( x > 1.0 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� parametru 'X' musi by� <= 1.0",
                    "Niepoprawny parametr!",
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }
        return true;
    }

    /**
     *  Publiczna metoda ��cz�ca warto�� ka�dego pixela bitmapy wej�ciowej
     *  i drugiej bitmapy przekazanej jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin1   bitmapa wej�ciowa
     *  @param bin2   bitmapa dodatkowa
     *  @return   bitmapa wyj�ciowa
     */
    public static JBitmap blending(JBitmap bin1, JBitmap bin2, double x) {
        // bitmapa wyj�ciowa
        JBitmap bout = new JBitmap( bin1.getWidth(), bin1.getHeight() );
        // do uzupe�nienia - pocz�tek 

        // do uzupe�nienia - koniec
        return bout;
    }


}