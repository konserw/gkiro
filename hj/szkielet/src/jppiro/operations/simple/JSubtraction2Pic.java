/*
 *  JSubtraction2Pic.java
 *
 *  Zawiera klas� JSubtraction2Pic,
 *  odejmuj�ca od warto�ci ka�dego piksela obrazu warto�� piksela drugiego obrazu.
 */

// definicja pakietu
package jppiro.operations.simple;

// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;

/**
 *  Publiczna klasa JSubtraction2Pic odejmuje od warto�ci ka�dego piksela obrazu
 *  warto�� piksela drugiego obrazu.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JSubtraction2Pic extends JOperation
{
    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JSubtraction2Pic() {
        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Subtraction 2Pic " );
        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class, JBitmap.class );
        setParamDesc( "Obraz podstawowy", "Drugi obraz" );
    }

    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {
        // wywo�anie funkcji subtraction2Pic()
        return (Object) subtraction2Pic( (JBitmap)param[0],
                                         (JBitmap)param[1] );
    }

    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {
        // obraz podstawowy przekazany jako pierwszy parametr
        JBitmap bin1 =(JBitmap) param[0];

        // obraz dodatkowy przekazany jako drugi parametr
        JBitmap bin2 =(JBitmap) param[1];

        // je�eli bitmapy r�ni� sie rozmiarami
        if( bin1.getWidth() != bin2.getWidth()  || bin1.getHeight() != bin2.getHeight() ) {
            JOptionPane.showMessageDialog( null,
                    "Obrazy maj� r�ne rozmiary",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }
        return true;
    }

    /**
     *  Publiczna metoda odejmuj�ca od warto�ci ka�dego pixela bitmapy warto�� 
     *  piksela drugiej bitmapy.
     *
     *  @param bin1   bitmapa wej�ciowa
     *  @param bin2   bitmapa dodatkowa
     *  @return   bitmapa wyj�ciowa
     */
    public JBitmap subtraction2Pic(JBitmap bin1, JBitmap bin2 ) {
        // bitmapa wyj�ciowa
        JBitmap bout = new JBitmap( bin1.getWidth(), bin1.getHeight() );
        // do uzupe�nienia - pocz�tek        

        // do uzupe�nienia - koniec
        return bout;
    }
}