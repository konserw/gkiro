/*
 *  JAddition.java
 *
 *  Zawiera klas� JAddition,
 *  dodaj�c� do warto�ci ka�dego piksela obrazu warto�� sta��.
 */

// definicja pakietu
package jppiro.operations.simple;

// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;

/**
 *  Publiczna klasa JAddition dodaje do warto�ci ka�dego piksela obrazu
 *  warto�� sta��.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JAddition extends JOperation
{
    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JAddition() {
        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Addition " );
        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class, Integer.class );
        setParamDesc( "Obraz podstawowy", "Warto�� dodawana do ka�dego piksela obrazu" );
    }

    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {
        // wywo�anie funkcji addition()
        return (Object) addition( (JBitmap)param[0],
                                  (Integer)param[1] );
    }

    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {
        // warto�� dodawana do ka�dego piksela obrazu przekazana jako drugi parametr
        int value =(Integer) param[1];

        // je�eli warto�� dodawana jest < 0
        if( value < 0 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� dodawana do ka�dego piksela musi by� >= 0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // je�eli warto�� dodawana jest > 255
        if( value > 255 ) {        
            JOptionPane.showMessageDialog( null,
                    "Warto�� dodawana do ka�dego piksela musi by� <= 255",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }
        return true;
    }

    /**
     *  Publiczna metoda dodaj�ca do warto�ci ka�dego pixela bitmapy
     *  warto�� sta�� przekazan� jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @param value   warto�� dodawana do warto�ci ka�dego pixela bitmapy
     *  @return   bitmapa wyj�ciowa
     */
    public static JBitmap addition(JBitmap bin, int value) {
        // bitmapa wyj�ciowa
        JBitmap bout = new JBitmap(bin.getWidth(), bin.getHeight());
        // do uzupe�nienia - pocz�tek        

        // do uzupe�nienia - koniec
        return bout;
    }
}