/*
 * JABS.java
 *
 * Created on 13 pa�dziernik 2007, 14:27
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package jppiro.operations.simple;

import javax.swing.JOptionPane;
import jppiro.algorithms.JBitmap;
import jppiro.operations.JOperation;

/**
 *
 * @author adam
 */
public class JABS extends JOperation
{
    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JABS() {
        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "ABS " );
        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class);
        setParamDesc( "Obraz podstawowy");
    }

    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {
        return (Object) abs( (JBitmap)param[0]);                        
    }

    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {
        return true;
    }

    /**
     *  Publiczna metoda obliczaj�ca warto�� bezwzgl�dn� ka�dego piksela.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @return   bitmapa wyj�ciowa
     */
    public static JBitmap abs(JBitmap bin) {
        // bitmapa wyj�ciowa
        JBitmap bout = new JBitmap( bin.getWidth(), bin.getHeight() );
        
        // tablice pikseli bitmapy wej�ciowej i wyj�ciowej
        int binBits[] = bin.getPixels();
        int boutBits[] = bout.getPixels();
        
        // dla ka�dego piksela bitmapy
        for(int i=0; i<binBits.length; i++) {
            boutBits[i] = Math.abs(binBits[i]);         
        }
        
        // dodanie do listy operacji na obrazie informacji o wykonanej operacji 
        bout.setOperations( bin );
        bout.addOperation( " abs" );
        
        return bout;
    }        


}