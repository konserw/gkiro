/*
 *  JDivision.java
 *
 *  Zawiera klas� JDivision,
 *  dziel�c� warto�c ka�dego piksela obrazu przez warto�c sta��.
 */

// definicja pakietu
package jppiro.operations.simple;

// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;

/**
 *  Publiczna klasa JDivision dzieli warto�� ka�dego piksela obrazu
 *  przez warto�c sta��.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JDivision extends JOperation
{
    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JDivision() {
        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Division " );
        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class, Double.class );
        setParamDesc( "Obraz podstawowy", "Warto�� dzielnika ka�dego piksela obrazu" );
    }

    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {
        // wywo�anie funkcji division()
        return (Object) division( (JBitmap)param[0],
                                  (Double)param[1] );
    }

    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {
        // warto�� dzielnika ka�dego piksela obrazu przekazana jako drugi parametr
        double value =(Double) param[1];

        // je�eli warto�� dzielnika jest < 1.0
        if( value < 1.0 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� dzielnika ka�dego piksela musi by� >= 1.0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // je�eli warto�� dzielnika jest > 10.0
        if( value > 10.0 ) {          
            JOptionPane.showMessageDialog( null,
                    "Warto�� dzielnika ka�dego piksela musi by� <= 10.0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }
        return true;
    }

    /**
     *  Publiczna metoda dziel�ca warto�� ka�dego pixela bitmapy 
     *  przez warto�� sta�� przekazan� jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @param value   warto�� dzielnika ka�dego pixela bitmapy
     *  @return   bitmapa wyj�ciowa
     */    
    public static JBitmap division(JBitmap bin, double value) {
        // bitmapa wyj�ciowa
        JBitmap bout = new JBitmap(bin.getWidth(), bin.getHeight());
        // do uzupe�nienia - pocz�tek        

        // do uzupe�nienia - koniec
        return bout;
    }
}