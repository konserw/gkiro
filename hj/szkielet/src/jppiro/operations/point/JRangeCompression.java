/*
 *  JRangeCompression.java
 *
 *  Zawiera klas� JRangeCompression,
 *  wykonuj�c� operacj� kompresji zakresu poziom�w szaro�ci obrazu wej�ciowego.
 */

// definicja pakietu
package jppiro.operations.point;

// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import static java.lang.Math.abs;
import static java.lang.Math.log;

/**
 *  Publiczna klasa JRangeCompression wykonuje operacj� kompresji zakresu
 *  poziom�w szaro�ci obrazu wej�ciowego.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JRangeCompression extends JOperation
{
    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JRangeCompression() {
        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Range Compresssion " );
        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class );
        setParamDesc( "Obraz podstawowy" );
    }

    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {
        // wywo�anie funkcji rangeCompression()
        return (Object) rangeCompression( (JBitmap)param[0] );
    }

    /**
     *  Publiczna metoda wykonuj�ca kompresj� zakres�w (logarytmowanie)
     *  zakresu poziom�w szaro�ci bitmapy wej�ciowej.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @return   bitmapa wyj�ciowa
     */
    public static JBitmap rangeCompression(JBitmap bin) {
        // bitmapa wyj�ciowa
        JBitmap bout = new JBitmap(bin.getWidth(), bin.getHeight());
        // do uzupe�nienia - pocz�tek        

        // do uzupe�nienia - koniec
        return bout;
    }
}