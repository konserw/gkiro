/*
 *  JNormalization.java
 *
 *  Zawiera klas� JNormalization,
 *  wykonuj�c� operacj� normalizacji (rozci�gni�cia/�cisni�cia zakresu poziom�w)
 *  obrazu wej�ciowego.
 */

// definicja pakietu
package jppiro.operations.point;

// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;

/**
 *  Publiczna klasa JNormalization wykonuje operacj� normalizacji
 *  (rozci�gni�cia/�cisni�cia zakresu poziom�w) obrazu wej�ciowego.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JNormalization extends JOperation
{
    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JNormalization() {
        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Normalization " );
        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class, Integer.class, Integer.class );
        setParamDesc( "Obraz podstawowy",
                      "Wej�ciowy minimalny poziom szaro�ci - R1",
                      "Wej�ciowy maksymalny poziom szaro�ci - R2" );
    }

    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {
        // wywo�anie funkcji normalization()
        return (Object) normalization( (JBitmap)param[0],
                                       (Integer)param[1],
                                       (Integer)param[2] );
    }

    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {
        // wej�ciowy minimalny poziom szaro�ci przekazany jako drugi parametr
        int r1=(Integer) param[1];

        // je�eli minimalny poziom szaro�ci jest < 0
        if( r1 < 0 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� minimalnego poziomu szaro�ci 'R1' musi by� >= 0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // je�eli minimalny poziom szaro�ci jest > 255
        if( r1 > 255 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� minimalnego poziomu szaro�ci 'R1' musi by� <= 255",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // wej�ciowy maksymalny poziom szaro�ci przekazany jako trzeci parametr
        int r2=(Integer) param[2];

        // je�eli maksymalny poziom szaro�ci jest < 0
        if( r2 < 0 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� maksymalnego poziomu szaro�ci 'R2' musi by� >= 0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // je�eli maksymalny poziom szaro�ci jest > 255
        if( r2 > 255 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� maksymalnego poziomu szaro�ci 'R2' musi by� <= 255",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // je�eli minimalny poziom szaro�ci jest >= od poziomu maksymalnego
        if(r1 >= r2) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� minimalnego poziomu szaro�ci 'R1' musi by� < 'R2'",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }
        return true;
    }

    /**
     *  Publiczna metoda wykonuj�ca normalizacj� (rozci�gni�cie/�cisni�cie 
     *  zakresu poziom�w szaro�ci) bitmapy wej�ciowej do poziomu przekazanego
     *  jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @param r1   minimalny poziom szaro�ci
     *  @param r2   maksymalny poziom szaro�ci
     *  @return   bitmapa wyj�ciowa
     */
    public static JBitmap normalization(JBitmap bin, int r1, int r2) {
        // bitmapa wyj�ciowa
        JBitmap bout = new JBitmap(bin.getWidth(), bin.getHeight());
        // do uzupe�nienia - pocz�tek        

        // do uzupe�nienia - koniec
        return bout;
    }
}