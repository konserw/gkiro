/*
 *  JNormalization.java
 *
 *  Zawiera klas� JNormalization,
 *  wykonuj�c� operacj� normalizacji (rozci�gni�cia/�cisni�cia zakresu poziom�w)
 *  obrazu wej�ciowego.
 */

// definicja pakietu
package jppiro.operations.point;

// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;

/**
 *  Publiczna klasa JNormalization wykonuje operacj� normalizacji
 *  (rozci�gni�cia/�cisni�cia zakresu poziom�w) obrazu wej�ciowego.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JNormalizationMinMax extends JOperation
{

    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JNormalizationMinMax() {
        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "NormalizationMinMax " );
        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class);
        setParamDesc( "Obraz podstawowy");
    }

    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {
        // wywo�anie funkcji normalization()
        return (Object) normalizationMinMax( (JBitmap)param[0]);
    }

    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    
    /**
     *  Publiczna metoda wykonuj�ca normalizacj� (rozci�gni�cie/�cisni�cie 
     *  zakresu poziom�w szaro�ci) bitmapy wej�ciowej.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @param r1   minimalny poziom szaro�ci
     *  @param r2   maksymalny poziom szaro�ci
     *  @return   bitmapa wyj�ciowa
     */
    public static JBitmap normalizationMinMax(JBitmap bin) {
        // bitmapa wyj�ciowa
        JBitmap bout = new JBitmap(bin.getWidth(), bin.getHeight());
        // do uzupe�nienia - pocz�tek        

        // do uzupe�nienia - koniec
        return bout;
    }
}