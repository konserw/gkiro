/*
 *  JContrastStretching.java
 *
 *  Zawiera klas� JContrastStretching,
 *  wykonuj�c� operacj� �ciskania/rozci�gania kontrastu obrazu wej�ciowego
 */

// definicja pakietu
package jppiro.operations.point;

// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;

/**
 *  Publiczna klasa JNormalization wykonuje operacje �ciskania/rozci�gania kontrastu 
 *  obrazu wej�ciowego.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JContrastStretching extends JOperation
{
    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JContrastStretching() {
        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Contrast Stretching " );
        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class, Integer.class, Integer.class, Integer.class, Integer.class );
        setParamDesc( "Obraz podstawowy",
                      "Wej�ciowy dolny zakres poziom�w szaro�ci - R1",
                      "Wej�ciowy g�rny zakres poziom�w szaro�ci - R2",
                      "Wyj�ciowy dolny zakres poziom�w szaro�ci - S1",
                      "Wyj�ciowy g�rny zakres poziom�w szaro�ci - S2" );
    }

    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {
        // wywo�anie funkcji contrastStretching()
        return (Object) contrastStretching( (JBitmap)param[0],
                                            (Integer)param[1],
                                            (Integer)param[2],
                                            (Integer)param[3],
                                            (Integer)param[4] );
    }

    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {
        // wej�ciowy minimalny poziom szaro�ci przekazany jako drugi parametr
        int r1=(Integer) param[1];

        // je�eli minimalny poziom szaro�ci jest < 0
        if( r1 < 0 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� minimalnego poziomu szaro�ci 'R1' musi by� >= 0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // je�eli minimalny poziom szaro�ci jest > 255
        if( r1 > 255 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� minimalnego poziomu szaro�ci 'R1' musi by� <= 255",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // wej�ciowy maksymalny poziom szaro�ci przekazany jako trzeci parametr
        int r2=(Integer) param[2];

        // je�eli maksymalny poziom szaro�ci jest < 0
        if( r2 < 0 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� maksymalnego poziomu szaro�ci 'R2' musi by� >= 0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // je�eli maksymalny poziom szaro�ci jest > 255
        if( r2 > 255 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� maksymalnego poziomu szaro�ci 'R2' musi by� <= 255",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // je�eli minimalny poziom szaro�ci jest >= od poziomu maksymalnego
        if(r1 >= r2) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� minimalnego poziomu szaro�ci 'R1' musi by� < 'R2'",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // wyj�ciowy minimalny poziom szaro�ci przekazany jako czwarty parametr
        int s1=(Integer) param[3];

        // je�eli minimalny poziom szaro�ci jest < 0
        if( s1 < 0 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� minimalnego poziomu szaro�ci 'S1' musi by� >= 0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // je�eli minimalny poziom szaro�ci jest > 255
        if( s1 > 255 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� minimalnego poziomu szaro�ci 'S1' musi by� <= 255",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // wyj�ciowy maksymalny poziom szaro�ci przekazany jako pi�ty parametr
        int s2=(Integer) param[4];

        // je�eli maksymalny poziom szaro�ci jest < 0
        if( s2 < 0 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� maksymalnego poziomu szaro�ci 'S2' musi by� >= 0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // je�eli maksymalny poziom szaro�ci jest > 255
        if( s2 > 255 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� maksymalnego poziomu szaro�ci 'S2' musi by� <= 255",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // je�eli minimalny poziom szaro�ci jest >= od poziomu maksymalnego
        if( s1 >= s2) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� minimalnego poziomu szaro�ci 'S1' musi by� < 'S2'",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }
        return true;
    }

    /**
     *  Publiczna metoda wykonuj�ca �ciskanie/rozci�ganie kontrastu 
     *  bitmapy wej�ciowej pomi�dzy poziomami przekazanymi jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @param r1   minimalny wej�ciowy poziom szaro�ci (r1 < min)
     *  @param r2   maksymalny wej��iowy poziom szaro�ci (r < max)
     *  @param s1   minimalny wyj�ciowy poziom szaro�ci (s1 >= 0)
     *  @param s2   maksymalny wyj�ciowy poziom szaro�ci (s2 <= 255)
     *  @return   bitmapa wyj�ciowa
     */ 
    public static JBitmap contrastStretching(JBitmap bin, int r1, int r2, int s1, int s2) {
        // bitmapa wyj�ciowa
        JBitmap bout = new JBitmap(bin.getWidth(), bin.getHeight());
        // do uzupe�nienia - pocz�tek        

        // do uzupe�nienia - koniec
        return bout;
    }
}