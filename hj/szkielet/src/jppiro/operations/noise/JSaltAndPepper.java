/*
 *  JSaltAndPepper.java
 *
 *  Zawiera klas� JSaltAndPepper,
 *  dodaj�c� do obrazu szum typu s�l i pieprz.
 */

// definicja pakietu
package jppiro.operations.noise;

// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import java.util.Random;
import javax.swing.JOptionPane;

/**
 *  Publiczna klasa JSaltAndPepper dodaje do obrazu szum typu s�l i pieprz.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JSaltAndPepper extends JOperation
{
    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JSaltAndPepper() {
        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "SaltAndPepper " );
        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class, Double.class, Double.class );
        setParamDesc( "Obraz podstawowy", "Prawdopodobie�stwo dla 'Salt'", 
                      "Prawdopodobie�stwo dla 'Pepper'" );
    }

    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {
        // wywo�anie funkcji gaussian()
        return (Object) saltAndPepper( (JBitmap)param[0],
                                       (Double)param[1],
                                       (Double)param[2] );
    }

    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {
        // prawdopodobie�stwo wyst�pienia soli przekazane jako drugi parametr
        double salt=(Double) param[1];

        // je�eli prawdopodobie�stwo 'soli' jest < 0.0
        if( salt < 0.0 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� parametru 'salt' musi by� >= 0.0",
                    "Niepoprawny parametr!",
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // je�eli prawdopodobie�stwo 'soli' jest > 1.0
        if( salt > 1.0 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� parametru 'salt' musi by� <= 1.0",
                    "Niepoprawny parametr!",
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // prawdopodobie�stwo wyst�pienia pieprzu przekazane jako trzeci parametr
        double pepper=(Double) param[2];

        // je�eli prawdopodobie�stwo 'pieprzu' jest < 0.0
        if(pepper < 0.0) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� parametru 'pepper' musi by� >= 0.0",
                    "Niepoprawny parametr!",
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // je�eli prawdopodobie�stwo 'pieprzu' jest > 1.0
        if(pepper > 1.0) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� parametru 'pepper' musi by� <= 1.0",
                    "Niepoprawny parametr!",
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }
        
        // je�eli ��czne prawdopodobie�stwo 'soli' i 'pieprzu' jest >= 1.0
        if(salt + pepper >= 1.0) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� sumy parametr�w 'salt' i pepper' musi by� < 1.0",
                    "Niepoprawny parametr!",
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }        
        return true;
    }

    /**
     *  Publiczna metoda dodaj�ca do warto�ci ka�dego pixela bitmapy
     *  warto�� szumu typu s�l i pieprz.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @param salt   prawdopodobienstwo wystapienia soli
     *  @param pepper   prawdopodobienstwo wystapienia pieprzu
     *  @return   bitmapa wyj�ciowa
     */
    public static JBitmap saltAndPepper(JBitmap bin, double salt, double pepper) {
        // bitmapa wyj�ciowa
        JBitmap bout = new JBitmap(bin.getWidth(), bin.getHeight());
        // inicjalizacja generatora
        Random rnd = new Random();
        // do uzupe�nienia - pocz�tek        

        // do uzupe�nienia - koniec
        return bout;
    }
}