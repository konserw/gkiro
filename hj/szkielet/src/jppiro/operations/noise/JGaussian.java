/*
 *  JGaussian.java
 *
 *  Zawiera klas� JGaussian,
 *  dodaj�c� do obrazu szum Gaussa.
 */

// definicja pakietu
package jppiro.operations.noise;

// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import java.util.Random;
import javax.swing.JOptionPane;

/**
 *  Publiczna klasa JGaussian dodaje do obrazu szum Gaussa.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JGaussian extends JOperation
{
    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JGaussian() {
        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Gaussian " );
        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class, Double.class, Double.class );
        setParamDesc( "Obraz podstawowy", "Warto�� �rednia", "Odchylenie standardowe" );
    }

    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {
        // wywo�anie funkcji gaussian()
        return (Object) gaussian( (JBitmap)param[0],
                                  (Double)param[1],
                                  (Double)param[2] );
    }

    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {
        // warto�� �rednia przekazana jako drugi parametr
        double mean =(Double) param[1];

        // je�eli warto�� �redniej jest < -255.0
        if( mean < -255.0 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� �redniej musi by� >= -255.0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // je�eli warto�� �redniej jest > 255.0
        if( mean > 255.0 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� �redniej musi by� <= 255.0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // odchylenie standardowe przekazane jako trzeci parametr
        double sDev =(Double) param[2];

        // je�eli warto�� odchylenia standardowego jest < 0.0
        if( sDev < 0.0 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� odchylenia musi by� >= 0.0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // je�eli warto�� odchylenia standardowego jest > 255.0
        if( sDev > 255.0 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� odchylenia musi by� <= 255.0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }
        return true;
    }

    /**
     *  Publiczna metoda dodaj�ca do warto�ci ka�dego pixela bitmapy
     *  warto�� szumu Gaussa.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @param mean   warto�� �rednia
     *  @param stDev   odchylenie standardowe
     *  @return   bitmapa wyj�ciowa
     */
    public static JBitmap gaussian(JBitmap bin, double mean, double stDev) {
        // bitmapa wyj�ciowa
        JBitmap bout = new JBitmap(bin.getWidth(), bin.getHeight());
        // inicjalizacja generatora
        Random rnd = new Random();
        // do uzupe�nienia - pocz�tek        

        // do uzupe�nienia - koniec
        return bout;
    }
}