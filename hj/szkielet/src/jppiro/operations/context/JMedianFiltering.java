/*
 *  JMedianFiltering.java
 *
 *  Zawiera klas� JMedianFiltering,
 *  wykonuj�c� filtracj� medianow�.
 */


// definicja pakietu
package jppiro.operations.context;


// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import java.util.Arrays;
import javax.swing.JOptionPane;




/**
 *  Publiczna klasa JMedianFiltering wykonuje fiktracj� medianow�.
 *  warto�� sta��.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JMedianFiltering extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JMedianFiltering() {

        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Median Filtering " );

        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class, Integer.class );
        setParamDesc( "Obraz podstawowy", "Rozmiar maska filtru" );
    }




    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo�anie funkcji medianFiltering()
        return (Object) medianFiltering( (JBitmap)param[0],
                                         (Integer)param[1] );
    }




    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {

        // rozmiar maski dla filtru przekazany jako drugi parametr
        int mask =(Integer) param[1];

        // je�eli rozmiar maski jest < 1
        if( mask < 1 ) {

            JOptionPane.showMessageDialog( null,
                    "Warto�� rozmiaru maski musi by� >= 1",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        // je�eli rozmiar maski jest > 11
        if( mask > 11 ) {

            JOptionPane.showMessageDialog( null,
                    "Warto�� rozmiaru maski musi by� <= 11",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        // je�eli rozmiar maski jest parzysty
        if( mask % 2 == 0 ) {

            JOptionPane.showMessageDialog( null,
                    "Warto�� rozmiaru maski musi by� nieparzysta",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        return true;
    }




    /**
     *  Publiczna metoda wykonuj�ca filtracj� medianow� mask� o rozmiarze
     *  przekazanym jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @param maskSize   rozmiar maski medianowej
     *  @return   bitmapa wyj�ciowa
     */
    public static JBitmap medianFiltering(JBitmap bin, int maskSize) {
        return bin;
    }


}