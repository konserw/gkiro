/*
 *  JHighpassFiltering.java
 *
 *  Zawiera klas� JHighpassFiltering,
 *  wykonuj�c� filtracj� kontekstow� (wyostrzaj�c�) wybran� mask�.
 */


// definicja pakietu
package jppiro.operations.context;


// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;




/**
 *  Publiczna klasa JHighpassFiltering wykonuje fiktracj� kontekstow� (wyostrzaj�c�)
 *  wybran� mask�.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JHighpassFiltering extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JHighpassFiltering() {

        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Highpass Filtering " );

        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class, JMask.class, Double.class );
        setParamDesc( "Obraz podstawowy", "Maska filtru", "Wsp�czynnik 'k'" );
    }




    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo�anie funkcji highpassFiltering()
        return (Object) highpassFiltering( (JBitmap)param[0],
                                           (JMask)param[1],
                                           (Double)param[2] );
    }




    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {

        // maska przekazana jako drugi parametr
        JMask mask =(JMask) param[1];

        // je�eli nie przekazano maski
        if( mask == null ) {

            JOptionPane.showMessageDialog( null,
                    "Nie przekazano maski!",
                    "Niepoprawny parametr!",
                    JOptionPane.WARNING_MESSAGE);

            return false;
        }

        // wsp�czynik 'k' przekazany jako trzeci parametr
        double k =(Double) param[2];

        // je�eli wsp�czynnik k <= 0
        if( k <= 0 ) {

            JOptionPane.showMessageDialog( null,
                    "Parametr 'k' musi by� > 0!",
                    "Niepoprawny parametr!",
                    JOptionPane.WARNING_MESSAGE);

            return false;
        }

        return true;
    }




    /**
     *  Publiczna metoda wykonuj�ca filtracj� kontekstow� (wyostrzaj�c�)
     *  mask� przekazan� jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @param mask   maska dla filtru kontekstowego
     *  @param k   wsp�czynnik 'k'
     *  @return   bitmapa wyj�ciowa
     */
    public static JBitmap highpassFiltering(JBitmap bin, JMask mask, double k) {

        // bitmapa wyj�ciowa i bitmapa r�nicy
        JBitmap bout = new JBitmap( bin );
        return bout;
    }


}