/*
 *  JGlobalThresholding.java
 *
 *  Zawiera klas� JGlobalThresholding,
 *  wykonuj�c� operacj� progowania obrazu progiem globalnym.
 */

// definicja pakietu
package jppiro.operations.thresholding;

// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;

/**
 *  Publiczna klasa JGlobalThresholding wykonuje operacj� progowania obrazu
 *  progiem globalnym.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JGlobalThresholding extends JOperation
{
    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JGlobalThresholding() {
        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Global Thresholding " );
        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class);
        setParamDesc( "Obraz podstawowy");
    }

    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {
        // wywo�anie funkcji globalThresholding()
        return (Object) globalThresholding( (JBitmap)param[0] );
    } 

    /**
     *  Publiczna metoda wykonuj�ca operacj� globalnego progowania obrazu
     *  przekazanego jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @return   bitmapa wyj�ciowa
     */ 
    public static JBitmap globalThresholding(JBitmap bin) {
        // bitmapa wyj�ciowa
        JBitmap bout = new JBitmap( bin.getWidth(), bin.getHeight() );
        // do uzupe�nienia - pocz�tek        

        // do uzupe�nienia - koniec
        return bout;
    }






}