 /*
 *  JDynamicThresholding.java
 *
 *  Zawiera klas� JDynamicThresholding,
 *  wykonuj�c� operacj� dynamicznego progowania obrazu.
 */

// definicja pakietu
package jppiro.operations.thresholding;

// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;

/**
 *  Publiczna klasa JDynamicThresholding wykonuje operacj� dynamicznego
 *  progowania obrazu.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JDynamicThresholding extends JOperation
{
    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JDynamicThresholding() {
        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Dynamic Thresholding " );
        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class, Integer.class, Integer.class );
        setParamDesc( "Obraz podstawowy", "Rozmiar maski otoczenia (nieparzysty >=3)",
                      "Sta�a do odj�cia" );
    }

    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {
        // wywo�anie funkcji dynamicThresholding()
        return (Object) dynamicThresholding( (JBitmap)param[0], 
                                             (Integer)param[1], 
                                             (Integer)param[2] );
    }

    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {
        // rozmiar maski otoczenia przekazany jako drugi parametr
        int maskSize =(Integer) param[1];

        // je�eli rozmiar maski jest < 3
        if( maskSize < 3 ) {
            JOptionPane.showMessageDialog( null,
                    "Rozmiar maski musi by� >= 3",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // je�eli rozmiar maski > 49
        if( maskSize > 49 ) {
            JOptionPane.showMessageDialog( null,
                    "Rozmiar maski musi by� <= 49",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // je�eli rozmiar maski jest parzysty
        if( maskSize % 2 == 0 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� rozmiaru maski musi byc nieparzysta",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // warto�� sta�a do odj�cia przekazana jako trzeci parametr
        int subConst=(Integer) param[2];

        // je�eli sta�a do odj�cia < 0
        if( subConst < 0 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� sta�ej do odj�cia musi by� >= 0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }

        // je�eli sta�a do odj�cia > 50
        if( subConst > 50 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto�� sta�ej do odj�cia musi by� <= 50",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );
            return false;
        }
        return true;
    }

    /**
     *  Publiczna metoda wykonuj�ca operacj� dynamicznego progowania obrazu
     *  przekazanego jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @param maskSize   rozmiar maski otoczenia (liczba nieparzysta >=3)
     *  @param subConst   sta�a do odjecia
     *  @return   bitmapa wyj�ciowa
     */ 
    public static JBitmap dynamicThresholding(JBitmap bin, int maskSize, int subConst) {
        // bitmapa wyj�ciowa
        JBitmap bout = new JBitmap( bin.getWidth(), bin.getHeight() );
        // do uzupe�nienia - pocz�tek        

        // do uzupe�nienia - koniec
        return bout;
    }
}