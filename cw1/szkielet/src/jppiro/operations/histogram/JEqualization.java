/*
 *  JEqualization.java
 *
 *  Zawiera klas? JEqualization,
 *  wyr?wnuj?c? histogram obrazu.
 */


// definicja pakietu
package jppiro.operations.histogram;


// importowane pakiety
import static java.lang.Math.log;
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import static java.lang.Math.round;




/**
 *  Publiczna klasa JEqualization wyr?wnuje histogram obrazu.
 *
 *  @author Micha? W?do?owski
 *  @version 1.0    (09/2006)
 */
public class JEqualization extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JEqualization() {

        // ustawienie nazwy funkcji (wy?wietlanej w menu programu)
        setName( "Equalization " );

        // ustawienie typ?w i opis?w parametr?w funkcji
        setParamTypes( JBitmap.class );
        setParamDesc( "Obraz podstawowy" );
    }




    /**
     *  Publiczna metoda wywo?uj?ca funkcj? z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo?anie funkcji equalization()
        return (Object) equalization( (JBitmap)param[0] );
    }




    /**
     *  Publiczna metoda wyr?wnuj?ca histogram obrazu wej?ciowego.
     *  Metoda statyczna - mo?e by? wywo?ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej?ciowa
     *  @return   bitmapa wyj?ciowa
     */
    public static JBitmap equalization(JBitmap bin) {

        int max = bin.getMax();
  //      double c = (255.0/log10(1 + max));
        
        for(int i=0; i< bin.getWidth(); ++i){
            for(int j=0; j< bin.getHeight(); ++j){
              //  double s = c*log(base, bin.getPixel(i, j)/log(a);
        //        double px = s > 255 ? 255 : s;
         //       int o = px < 0 ? 0 : (int)px;
         //       bin.setPixel(i, j, o);
            }
        }
        
        return bin;
    }


}