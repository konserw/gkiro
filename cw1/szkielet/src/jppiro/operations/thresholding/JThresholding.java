/*
 *  JThresholding.java
 *
 *  Zawiera klas� JThresholding,
 *  wykonuj�c� operacj� progowania obrazu.
 */


// definicja pakietu
package jppiro.operations.thresholding;


// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;




/**
 *  Publiczna klasa JThresholding wykonuje operacj� progowania obrazu.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JThresholding extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JThresholding() {

        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Thresholding " );

        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class, Integer.class );
        setParamDesc( "Obraz podstawowy", "Warto�� progu" );
    }




    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo�anie funkcji thresholding()
        return (Object) thresholding( (JBitmap)param[0],
                                      (Integer)param[1] );
    }




    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {

        // warto�� progu przekazana jako drugi parametr
        int thresholding =(Integer) param[1];

        // je�eli warto�� progu < 0
        if( thresholding < 0 ) {

            JOptionPane.showMessageDialog( null,
                    "Warto�� progu musi by� >= 0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        // je�eli warto�� progu > 255
        if( thresholding > 255 ) {

            JOptionPane.showMessageDialog( null,
                    "Warto�� progu musi by� <= 255",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        return true;
    }




    /**
     *  Publiczna metoda wykonuj�ca progowanie obrazu przekazanego jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @param threshold   warto�� progu
     *  @return   bitmapa wyj�ciowa
     */ 
    public static JBitmap thresholding(JBitmap bin, int threshold) {

        // bitmapa wyj�ciowa
        JBitmap bout = new JBitmap( bin.getWidth(), bin.getHeight() );

        // tablice pikseli bitmapy wej�ciowej i wyj�ciowej
        return bout;
    }


}