/*
 *  JAverageFiltering.java
 *
 *  Zawiera klas� JAverageFiltering,
 *  wykonuj�c� filtracj� kontekstow� (u�redniaj�c�) wybran� mask�.
 */


// definicja pakietu
package jppiro.operations.context;


// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;




/**
 *  Publiczna klasa JAverageFiltering wykonuje fiktracj� kontekstow� (u�redniaj�c�)
 *  wybran� mask�.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JAverageFiltering extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JAverageFiltering() {

        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Average Filtering " );

        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class, JMask.class );
        setParamDesc( "Obraz podstawowy", "Maska filtru" );
    }




    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo�anie funkcji averageFiltering()
        return (Object) averageFiltering( (JBitmap)param[0],
                                          (JMask)param[1] );
    }




    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {

        // maska przekazana jako drugi parametr
        JMask mask =(JMask) param[1];

        // je�eli nie przekazano maski
        if( mask == null ) {

            JOptionPane.showMessageDialog( null,
                    "Nie przekazano maski",
                    "Niepoprawny parametr!",
                    JOptionPane.WARNING_MESSAGE);

            return false;
        }

        return true;
    }




    /**
     *  Publiczna metoda wykonuj�ca filtracj� kontekstow� (u�redniaj�c�)
     *  mask� przekazan� jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @param mask   maska dla filtru kontekstowego
     *  @return   bitmapa wyj�ciowa
     */
    public static JBitmap averageFiltering(JBitmap bin, JMask mask) {

        JBitmap bout=JConvole.convole(bin, mask);
        
        for(int x=0;x<bout.getWidth();x++)
            for(int y=0;y<bout.getHeight();y++)
                bout.setPixel(x, y,(int)( bout.getPixel(x, y)/mask.getNorm()));
        
        return bout;
    }


}