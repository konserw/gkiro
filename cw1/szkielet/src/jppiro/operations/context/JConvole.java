/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jppiro.operations.context;

import jppiro.algorithms.JBitmap;
import jppiro.algorithms.JMask;

/**
 *
 * @author adam
 */
public class JConvole {

    public static JBitmap convole(JBitmap bin, JMask mask) {

        JBitmap bout = new JBitmap(bin);

        for (int x = mask.getSize() / 2; x < bout.getWidth() - mask.getSize() / 2; x++) {
            for (int y = mask.getSize() / 2; y < bout.getHeight() - mask.getSize() / 2; y++) {
                int sum = 0;
                for (int i = 0; i < mask.getSize(); i++) {
                    for (int j = 0; j < mask.getSize(); j++) {
                        sum += bin.getPixel(x - i + mask.getSize() / 2, y - j + mask.getSize() / 2) * mask.getMask(i, j);
                    }
                }

                bout.setPixel(x, y, sum);
            }
        }
        return bout;
    }
}
