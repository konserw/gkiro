/*
 *  JPower.java
 *
 *  Zawiera klas? JPower,
 *  wykonuj?c? operacj? podnoszenia do pot?gi poziom?w szaro?ci obrazu wej?ciowego.
 */


// definicja pakietu
package jppiro.operations.point;


// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;
import static java.lang.Math.pow;




/**
 *  Publiczna klasa JPower wykonuje operacj? podnoszenia do pot?gi 
 *  poziom?w szaro?ci obrazu wej?ciowego.
 *
 *  @author Micha? W?do?owski
 *  @version 1.0    (09/2006)
 */
public class JPower extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JPower() {

        // ustawienie nazwy funkcji (wy?wietlanej w menu programu)
        setName( "Power " );

        // ustawienie typ?w i opis?w parametr?w funkcji
        setParamTypes( JBitmap.class, Double.class );
        setParamDesc( "Obraz podstawowy", "Warto?? pot?gi" );
    }




    /**
     *  Publiczna metoda wywo?uj?ca funkcj? z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo?anie funkcji power()
        return (Object) power( (JBitmap)param[0],
                               (Double)param[1] );
    }




    /**
     *  Publiczna metoda weryfikuj?ca poprawno?? parametr?w.
     *
     *  @param param   parametry funkcji kt?re podlegaj? sprawdzeniu
     *  @return   true - je?eli parametry s? poprawne,
     *              false - je?li parametry nie s? poprawne
     */
    public boolean paramVerification(Object ... param) {

        // warto?? pot?gi przekazana jako drugi parametr
        double pow =(Double) param[1];

        // je?eli warto?? pot?gi jest <= 0.0
        if( pow <= 0.0 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto?? pot?gi musi by? > 0.0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        // je?eli warto?? pot?gi jest > 5.0
        if( pow > 5.0 ) {

            JOptionPane.showMessageDialog( null,
                    "Warto?? pot?gi musi by? <= 5.0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        return true;
    }




    /**
     *  Publiczna metoda wykonuj?ca podniesienie do pot?gi przekazanej jako
     *  parametr ka?dego piksela obraze wej?ciowego.
     *  Metoda statyczna - mo?e by? wywo?ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej?ciowa
     *  @param a   warto?? pot?gi
     *  @return   bitmapa wyj?ciowa
     */ 
    public static JBitmap power(JBitmap bin, double a) {
        int max = bin.getMax();
        double c = (255.0/pow(max, a));
        
        for(int i=0; i< bin.getWidth(); ++i){
            for(int j=0; j< bin.getHeight(); ++j){
                double s = c*pow(bin.getPixel(i, j), a);
                double px = s > 255 ? 255 : s;
                int o = px < 0 ? 0 : (int)px;
                bin.setPixel(i, j, o);
            }
        }
        return bin;
    }


}