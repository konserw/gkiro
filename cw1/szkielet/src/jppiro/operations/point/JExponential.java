/*
 *  JExponential.java
 *
 *  Zawiera klas? JExponential,
 *  wykonuj?c? operacj? funkcji wyk?adniczej na poziomach szaro?ci obrazu wej?ciowego.
 */


// definicja pakietu
package jppiro.operations.point;


// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;
import static java.lang.Math.pow;




/**
 *  Publiczna klasa JExponential wykonuje operacj? funkcji wyk?adniczej
 *  na poziomach szaro?ci obrazu wej?ciowego.
 *
 *  @author Micha? W?do?owski
 *  @version 1.0    (09/2006)
 */
public class JExponential extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JExponential() {

        // ustawienie nazwy funkcji (wy?wietlanej w menu programu)
        setName( "Exponential " );

        // ustawienie typ?w i opis?w parametr?w funkcji
        setParamTypes( JBitmap.class, Double.class );
        setParamDesc( "Obraz podstawowy", "Podstawa" );
    }




    /**
     *  Publiczna metoda wywo?uj?ca funkcj? z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo?anie funkcji exponential()
        return (Object) exponential( (JBitmap)param[0],
                                     (Double)param[1] );
    }




    /**
     *  Publiczna metoda weryfikuj?ca poprawno?? parametr?w.
     *
     *  @param param   parametry funkcji kt?re podlegaj? sprawdzeniu
     *  @return   true - je?eli parametry s? poprawne,
     *              false - je?li parametry nie s? poprawne
     */
    public boolean paramVerification(Object ... param) {

        // warto?? podstawy przekazana jako drugi parametr
        double base =(Double) param[1];

        // je?eli warto?? podstawy jest <= 0.0
        if( base <= 0.0 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto?? pot?gi musi by? > 0.0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        // je?eli warto?? podstawy jest == 1.0
        if( base == 1.0 ) {
            JOptionPane.showMessageDialog( null,
                    "Warto?? podstawy musi by? != 1.0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        // je?eli warto?? podstawy jest > 5.0
        if( base > 5.0 ) {

            JOptionPane.showMessageDialog( null,
                    "Warto?? pot?gi musi by? <= 5.0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        return true;
    }




    /**
     *  Publiczna metoda wykonuj?ca funkcje wyk?adnicz? na podstawie
     *  przekazanej jako parametr i bitmapie wej?ciowej.
     *  Metoda statyczna - mo?e by? wywo?ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej?ciowa
     *  @param base   warto?? podstawy
     *  @return   bitmapa wyj?ciowa
     */ 
    public static JBitmap exponential(JBitmap bin, double base) {
        int max = bin.getMax();
        double c = (255.0/pow(base,max));
        
        for(int i=0; i< bin.getWidth(); ++i){
            for(int j=0; j< bin.getHeight(); ++j){
                double s = c*pow(base, bin.getPixel(i, j));
                double px = s > 255 ? 255 : s;
                int o = px < 0 ? 0 : (int)px;
                bin.setPixel(i, j, o);
            }
        }
        return bin;
    }


}