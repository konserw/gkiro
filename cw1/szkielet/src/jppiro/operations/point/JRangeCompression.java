/*
 *  JRangeCompression.java
 *
 *  Zawiera klas? JRangeCompression,
 *  wykonuj?c? operacj? kompresji zakresu poziom?w szaro?ci obrazu wej?ciowego.
 */


// definicja pakietu
package jppiro.operations.point;


// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import static java.lang.Math.abs;
import static java.lang.Math.log;
import static java.lang.Math.pow;




/**
 *  Publiczna klasa JRangeCompression wykonuje operacj? kompresji zakresu
 *  poziom?w szaro?ci obrazu wej?ciowego.
 *
 *  @author Micha? W?do?owski
 *  @version 1.0    (09/2006)
 */
public class JRangeCompression extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JRangeCompression() {

        // ustawienie nazwy funkcji (wy?wietlanej w menu programu)
        setName( "Range Compresssion " );

        // ustawienie typ?w i opis?w parametr?w funkcji
        setParamTypes( JBitmap.class );
        setParamDesc( "Obraz podstawowy" );
    }




    /**
     *  Publiczna metoda wywo?uj?ca funkcj? z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo?anie funkcji rangeCompression()
        return (Object) rangeCompression( (JBitmap)param[0] );
    }




    /**
     *  Publiczna metoda wykonuj?ca kompresj? zakres?w (logarytmowanie)
     *  zakresu poziom?w szaro?ci bitmapy wej?ciowej.
     *  Metoda statyczna - mo?e by? wywo?ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej?ciowa
     *  @return   bitmapa wyj?ciowa
     */
    public static JBitmap rangeCompression(JBitmap bin) {
        double a = 5;
        int max = bin.getMax();
        double c = (255.0/Math.log10(1 + max));
        
        for(int i=0; i< bin.getWidth(); ++i){
            for(int j=0; j< bin.getHeight(); ++j){
                double s = c*log(1 + abs(bin.getPixel(i, j)))/log(a);
                double px = s > 255 ? 255 : s;
                int o = px < 0 ? 0 : (int)px;
                bin.setPixel(i, j, o);
            }
        }
        return bin;
    }


}