/*
 *  JDivision2Pic.java
 *
 *  Zawiera klas? JDivision2Pic,
 *  dziel?c? warto?? ka?dego piksela obrazu przez warto?? piksela drugiego obrazu.
 */


// definicja pakietu
package jppiro.operations.simple;


// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;




/**
 *  Publiczna klasa JDivision2Pic dzieli warto?? ka?dego piksela obrazu
 *  przez warto?? piksela drugiego obrazu.
 *
 *  @author Micha? W?do?owski
 *  @version 1.0    (09/2006)
 */
public class JDivision2Pic extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JDivision2Pic() {

        // ustawienie nazwy funkcji (wy?wietlanej w menu programu)
        setName( "Division 2Pic " );

        // ustawienie typ?w i opis?w parametr?w funkcji
        setParamTypes( JBitmap.class, JBitmap.class );
        setParamDesc( "Obraz podstawowy", "Drugi obraz" );
    }




    /**
     *  Publiczna metoda wywo?uj?ca funkcj? z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo?anie funkcji division2Pic()
        return (Object) division2Pic( (JBitmap)param[0],
                                      (JBitmap)param[1] );
    }




    /**
     *  Publiczna metoda weryfikuj?ca poprawno?? parametr?w.
     *
     *  @param param   parametry funkcji kt?re podlegaj? sprawdzeniu
     *  @return   true - je?eli parametry s? poprawne,
     *              false - je?li parametry nie s? poprawne
     */
    public boolean paramVerification(Object ... param) {

        // obraz podstawowy przekazany jako pierwszy parametr
        JBitmap bin1 =(JBitmap) param[0];

        // obraz dodatkowy przekazany jako drugi parametr
        JBitmap bin2 =(JBitmap) param[1];

        // je?eli bitmapy r??ni? sie rozmiarami
        if( bin1.getWidth() != bin2.getWidth()  || bin1.getHeight() != bin2.getHeight() ) {

            JOptionPane.showMessageDialog( null,
                    "Obrazy maj? r??ne rozmiary",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        return true;
    }




    /**
     *  Publiczna metoda dziel?ca warto?? ka?dego pixela bitmapy przez warto?? 
     *  piksela drugiej bitmapy przekazanej jako parametr.
     *  Metoda statyczna - mo?e by? wywo?ana bez tworzenia obiektu.
     *
     *  @param bin1   bitmapa wej?ciowa
     *  @param bin2   bitmapa dodatkowa
     *  @return   bitmapa wyj?ciowa
     */
    public static JBitmap division2Pic(JBitmap bin1, JBitmap bin2) {

        // bitmapa wyj?ciowa
        JBitmap bout = new JBitmap( bin1.getWidth(), bin1.getHeight() );
        
        for(int i=0; i< bin1.getWidth(); ++i){
            for(int j=0; j< bin1.getHeight(); ++j){
                double px = (double)bin1.getPixel(i, j) / 255;
                int px2 = bin2.getPixel(i, j);
                double value = px2 == 0 ? 1.0 : px / ((double)px2 / 255);
                
                bout.setPixel(i, j, (int)(value*255));
            }
        }
         return bout;
    }


}