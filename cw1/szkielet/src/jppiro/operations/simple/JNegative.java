/*
 *  JNegative.java
 *
 *  Zawiera klas? JNegative,
 *  obliczaj?ca negatywa obrazu.
 */


// definicja pakietu
package jppiro.operations.simple;


// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;




/**
 *  Publiczna klasa JNegative oblicza negatywa obrazu.
 *
 *  @author Micha? W?do?owski
 *  @version 1.0    (09/2006)
 */
public class JNegative extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JNegative() {

        // ustawienie nazwy funkcji (wy?wietlanej w menu programu)
        setName( "Negative " );

        // ustawienie typ?w i opis?w parametr?w funkcji
        setParamTypes( JBitmap.class );
        setParamDesc( "Obraz podstawowy" );
    }




    /**
     *  Publiczna metoda wywo?uj?ca funkcj? z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo?anie funkcji negative()
        return (Object) negative( (JBitmap)param[0] );
    }




    /**
     *  Publiczna metoda zwracaj?ca negatyw bitmapy przekazanej jako parametr.
     *  Metoda statyczna - mo?e by? wywo?ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej?ciowa
     *  @return   bitmapa wyj?ciowa
     */
    public static JBitmap negative(JBitmap bin) {
        for(int i=0; i< bin.getWidth(); ++i){
            for(int j=0; j< bin.getHeight(); ++j){
                bin.setPixel(i, j, 255-bin.getPixel(i, j));
            }
        }
        return bin;
    }


}