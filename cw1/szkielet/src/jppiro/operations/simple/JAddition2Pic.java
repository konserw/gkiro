/*
 *  JAddition2Pic.java
 *
 *  Zawiera klas� JAddition2Pic,
 *  dodaj�c� do warto�ci ka�dego piksela obrazu warto�� piksela drugiego obrazu.
 */


// definicja pakietu
package jppiro.operations.simple;


// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;




/**
 *  Publiczna klasa JAddition2Pic dodaje do warto�ci ka�dego piksela obrazu
 *  warto�� piksela drugiego obrazu.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JAddition2Pic extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JAddition2Pic() {

        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Addition 2Pic " );

        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class, JBitmap.class );
        setParamDesc( "Obraz podstawowy", "Drugi obraz" );
    }




    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo�anie funkcji addition2Pic()
        return (Object) addition2Pic( (JBitmap)param[0],
                                      (JBitmap)param[1] );
    }




    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {

        // obraz podstawowy przekazany jako pierwszy parametr
        JBitmap bin1 =(JBitmap) param[0];

        // obraz dodatkowy przekazany jako drugi parametr
        JBitmap bin2 =(JBitmap) param[1];

        // je�eli bitmapy r�ni� sie rozmiarami
        if( bin1.getWidth() != bin2.getWidth()  || bin1.getHeight() != bin2.getHeight() ) {

            JOptionPane.showMessageDialog( null,
                    "Obrazy maj� r�ne rozmiary",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        return true;
    }




    /**
     *  Publiczna metoda dodaj�ca do warto�ci ka�dego pixela bitmapy warto�� 
     *  piksela drugiej bitmapy przekazanej jako parametr.
     *
     *  @param bin1   bitmapa wej�ciowa
     *  @param bin2   bitmapa dodatkowa
     *  @return   bitmapa wyj�ciowa
     */
    public JBitmap addition2Pic(JBitmap bin1, JBitmap bin2 ) {

        // bitmapa wyj�ciowa
        JBitmap bout = new JBitmap( bin1.getWidth(), bin1.getHeight() );

        return bout;
    }


}