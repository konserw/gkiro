/*
 *  JMultiplication2Pic.java
 *
 *  Zawiera klas� JMultiplication2Pic,
 *  mno��c� warto�� ka�dego piksela obrazu przez warto�� piksela drugiego obrazu.
 */


// definicja pakietu
package jppiro.operations.simple;


// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;




/**
 *  Publiczna klasa JMultiplication2Pic mno�y warto�ci ka�dego piksela obrazu
 *  przez warto�� piksela drugiego obrazu.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JMultiplication2Pic extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JMultiplication2Pic() {

        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Multiplication 2Pic " );

        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class, JBitmap.class );
        setParamDesc( "Obraz podstawowy", "Drugi obraz" );
    }




    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo�anie funkcji multiplication2Pic()
        return (Object) multiplication2Pic( (JBitmap)param[0],
                                            (JBitmap)param[1] );
    }




    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {

        // obraz podstawowy przekazany jako pierwszy parametr
        JBitmap bin1 =(JBitmap) param[0];

        // obraz dodatkowy przekazany jako drugi parametr
        JBitmap bin2 =(JBitmap) param[1];

        // je�eli bitmapy r�ni� sie rozmiarami
        if( bin1.getWidth() != bin2.getWidth()  || bin1.getHeight() != bin2.getHeight() ) {

            JOptionPane.showMessageDialog( null,
                    "Obrazy maj� r�ne rozmiary",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        return true;
    }




    /**
     *  Publiczna metoda mno��c� warto�� ka�dego pixela bitmapy przez warto�� 
     *  piksela drugiej bitmapy przekazanej jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin1   bitmapa wej�ciowa
     *  @param bin2   bitmapa dodatkowa
     *  @return   bitmapa wyj�ciowa
     */
    public static JBitmap multiplication2Pic(JBitmap bin1, JBitmap bin2) {

        // bitmapa wyj�ciowa
        JBitmap bout = new JBitmap( bin1.getWidth(), bin1.getHeight() );

        return bout;
    }


}