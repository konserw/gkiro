/*
 * JTrainBayesNormal.java
 *
 * Created on 25 listopad 2006, 02:33
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package jppiro.operations.trainStatisticalClassifier;

import java.util.LinkedList;
import java.util.List;
import jppiro.algorithms.JMatrix;
import jppiro.algorithms.JSamplesList;
import jppiro.algorithms.JStatSamplesSet;
import jppiro.algorithms.JVector;

/**
 *
 * @author adam
 */
public class JTrainBayesNormal {
    
    /** Creates a new instance of JTrainBayesNormal */
    public static List<JNormalParams> JTrainBayesNormal(JStatSamplesSet trainSet) {
        
        LinkedList<JNormalParams> paramList=new LinkedList<JNormalParams>();
        
        for(int i=0;i<trainSet.getSetSize();i++)
            paramList.add(normalParams(trainSet.getSamplesList(i),trainSet.countSamples()));
        
        return paramList;
        
    }
    
    private static JNormalParams normalParams(JSamplesList samples, int totalSamples) {
        
        JVector vector=new JVector(samples.getVectorSize());
        
        for(int i=0;i<samples.getListSize();i++)
            vector.add(samples.getSampleVector(i));
        
        vector.div(samples.getListSize());              
       
        JMatrix matrixM=new JMatrix(vector);
        
        JMatrix matrixC=new JMatrix(samples.getVectorSize(),samples.getVectorSize());
        
        for(int i=0;i<samples.getVectorSize();i++)
            for(int j=0;j<samples.getVectorSize();j++) {
                matrixC.setVal(i,j,covariance(samples.getFeatureVariable(i),
                        samples.getFeatureVariable(j)));
            }
        
        return new JNormalParams(matrixM, matrixC,samples.getListSize()/(double)totalSamples);
    }
    
    
    private static double covariance(double[] var1, double[] var2) {
      double[] mulVar1Var2=new double[var1.length];
      for(int i=0;i<var1.length;i++)
          mulVar1Var2[i]=var1[i]*var2[i];
      
      return mean(mulVar1Var2)-mean(var1)*mean(var2);
      
    }
    
    private static double mean(double[] vals) {
        double mean=0;
        for(int i=0;i<vals.length;i++)
            mean+=vals[i];
        mean/=vals.length;
        return mean;
    }
    
}
