/*
 * JTestKNN.java
 *
 * Created on 24 listopad 2006, 22:56
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package jppiro.operations.testStatisticalClassifier;

import java.util.Collections;
import java.util.LinkedList;
import jppiro.algorithms.JSample;
import jppiro.algorithms.JStatSamplesSet;
import jppiro.algorithms.JVector;

/**
 *
 * @author adam
 */
public class JTestKNN {
    
    /** Creates a new instance of JTestKNN */
    
    static class JDistance implements Comparable<JDistance> {
        public double distance;
        public int  classNumber;
        
        public JDistance(double distance, int classNumber) {
            this.distance=distance;
            this.classNumber=classNumber;
                    
        }
        
        public int compareTo(JDistance distance) {
            if(this.distance<distance.distance)
                return -1;
            else if(this.distance==distance.distance)         
                return 0;
            else return 1;
        }
    }
    
    
    public static int testKNN(int k,JStatSamplesSet trainSet, JVector vector) {
        
        LinkedList<JDistance> trainVectors=new LinkedList<JDistance>();
        JSample[] trainSamples=trainSet.getAllSamples();
        for(int i=0;i<trainSamples.length;i++)
            trainVectors.add(new JDistance(vector.distance(trainSamples[i].getVector()),
                    trainSamples[i].getSampleClass()));
        
        Collections.sort(trainVectors);
        
        int max=0;
        
        /*
         Tutaj nale�y uzupe�ni�
         */
        
        return max;
    }
    
}
