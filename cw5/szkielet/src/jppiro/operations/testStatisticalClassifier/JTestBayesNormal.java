/*
 * JTestBayeNormal.java
 *
 * Created on 25 listopad 2006, 03:18
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package jppiro.operations.testStatisticalClassifier;

import jppiro.algorithms.JMatrix;
import jppiro.algorithms.JVector;
import jppiro.operations.trainStatisticalClassifier.JNormalParams;

/**
 *
 * @author adam
 */
public class JTestBayesNormal {
    

    public static int JTestBayesNormal(JNormalParams[] normalsParams, JVector vector) {
        int classNumber=0;
        
      /*
       * Tutaj nale�y uzupe�ni�
       */
        
        return classNumber;
    }
    
    private static double d(JNormalParams normalParams, JVector vector) {
        JMatrix M=normalParams.M;
        JMatrix C=normalParams.C;
        
        JMatrix tmp1=(new JMatrix(vector)).sub(M);
        JMatrix tmp2=tmp1.calculateTranspose();
        
        tmp2=tmp2.mul(C.calculateInvert());
        tmp2=tmp2.mul(tmp1);
        
        return Math.log(normalParams.pw)-0.5*Math.log(C.calculateDeterminant())-0.5*tmp2.getVal(0,0);
        
    }
    
}
