/*
 *  JFourierTransform.java
 *
 *  Zawiera klas� JFourierTransform,
 *  wykonuj�c� przekszta�cenie Fouriera (DFT).
 */


// definicja pakietu
package jppiro.operations.transform;


// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.PI;





/**
 *  Publiczna klasa JFourierTransform wykonuje przekszta�cenie Fouriera (DFT).
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JFourierTransform extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JFourierTransform() {

        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Fourier Transform " );

        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class );
        setParamDesc( "Obraz podstawowy" );
    }




    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo�anie funkcji fourierTransform()
        return (Object) fourierTransform( (JBitmap)param[0] );
    }




    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {

        // obraz przekazany jako pierwszy parametr
        JBitmap bin =(JBitmap) param[0];

        // je�eli wymiary s� r�ne
        if( bin.getWidth() != bin.getHeight() ) {

            JOptionPane.showMessageDialog( null,
                    "Obraz nie jest kwadratowy",
                    "Niepoprawny parametr!",
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        // szukanie m takiego �e 2^m = wymiar obrazu
        int m =(int) ( Math.log(bin.getWidth()) / Math.log(2) );

        // je�eli wymiar nie jest pot�g� 2
        if( bin.getWidth() != ( 1 << m ) ) {

            JOptionPane.showMessageDialog( null,
                    "Wymiary obrazu nie s� pot�g� 2",
                    "Niepoprawny parametr!",
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        return true;
    }




    /**
     *  Publiczna metoda wykonuj�ca przekszta�cenie Fouriera (DFT) na obrazie
     *  przekazanym jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @return   bitmapa wyj�ciowa
     */ 
    public static JTransform fourierTransform(JBitmap bin) {

        // zmienne pomocnicze
        // obraz wyj�ciowy (zespolony)
        JTransform out = new JTransform( bin.getWidth(), bin.getHeight() );

        return out;
    }


}