/*
 *  JInverseFourierTransform.java
 *
 *  Zawiera klas� JInverseFourierTransform,
 *  wykonuj�c� odwrotne przekszta�cenie Fouriera (IDFT).
 */


// definicja pakietu
package jppiro.operations.transform;


// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.PI;




/**
 *  Publiczna klasa JInverseFourierTransform wykonuje odwrotne przekszta�cenie 
 *  Fouriera (IDFT).
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JInverseFourierTransform extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JInverseFourierTransform() {

        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Inverse Fourier Transform " );

        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JTransform.class );
        setParamDesc( "Transformata Fouriera" );
    }




    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo�anie funkcji inverseFourierTransform()
        return (Object) inverseFourierTransform( (JTransform)param[0] );
    }




    /**
     *  Publiczna metoda wykonuj�ca odwrotne przekszta�cenie Fouriera (IDFT) 
     *  na transformacie przekazanej jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param transform   dyskretna transformata Fouriera
     *  @return   bitmapa wyj�ciowa
     */ 
    public static JBitmap inverseFourierTransform(JTransform transform) {

        // zmienne pomocnicze
        double real, imag, tmp, angle;

        // cechy obrazu wyj�ciowego
        int width = transform.getWidth();
        int height = transform.getHeight();

        // obraz wyj�ciowy i jego tablica pikseli
        JBitmap bout = new JBitmap( width, height );
        return bout;
    }


}