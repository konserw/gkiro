/*
 *  JSkeletonStep.java
 *
 *  Zawiera klas� JSkeletonStep,
 *  wykonuj�c� pojedy�czy krok szkieletyzacji obrazu wybranymi elementami strukturalnymi.
 */


// definicja pakietu
package jppiro.operations.morphology;


import java.awt.Point;
import java.awt.Rectangle;
import java.util.HashSet;
import java.util.Set;
import jppiro.algorithms.*;
import jppiro.operations.*;

/**
 *  Publiczna klasa JSkeletonStep wykonuje pojedy�czy krok szkieletyzacji
 *  obrazu wybranymi elementami strukturalnymi.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JSkeletonStep extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JSkeletonStep() {

        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Skeleton Step " );

        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class );
        setParamDesc( "Obraz podstawowy");
                                        
    }




    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo�anie funkcji skeletonStep()
        return (Object) skeletonStep( (JBitmap)param[0]);
    }




    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {


        return true;
    }




    /**
     *  Publiczna metoda wykonuj�ca pojedy�czy krok szkieletyzacji obrazu
     *  elementami strukturalnymi przekazanymi jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @param elStr1   element strukturalny KA
     *  @param elStr2   element strukturalny KB
     *  @param elStr3   element strukturalny RA
     *  @param elStr4   element strukturalny RB
     *  @return   bitmapa wyj�ciowa
     */
    public static JBitmap skeletonStep(JBitmap bin) {

        Set<Point> image=JSkeletonStep.skeletonStep(bin.getObjectPoints(),bin.getImageRectangle());
        // bitmapa wyj�ciowa i pomocnicza
 
        JBitmap bout = new JBitmap(image, bin.getWidth(), bin.getHeight());

        // zwr�cenie bitmapy po wykonaniu operacji
        return bout;
    }

    public static Set<Point> skeletonStep(Set<Point> image, Rectangle imageRectangle) {
        return image;
    }

    protected void finalize() throws Throwable {
    }

}