/*
 *  JSkeleton.java
 *
 *  Zawiera klas� JSkeleton,
 *  wykonuj�c� szkieletyzacj� obrazu wybranymi elementami strukturalnymi.
 */


// definicja pakietu
package jppiro.operations.morphology;


import java.awt.Point;
import java.awt.Rectangle;
import java.util.Set;
import jppiro.algorithms.*;
import jppiro.operations.*;

/**
 *  Publiczna klasa JSkeleton wykonuje szkieletyzacj� obrazu wybranymi
 *  elementami strukturalnymi.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JSkeleton extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JSkeleton() {

        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Skeleton " );

        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class );
        setParamDesc(new String[] { "Obraz podstawowy"});
                               
    }




    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo�anie funkcji skeleton()
        return (Object) skeleton( (JBitmap)param[0]);
    }




    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {

        // pierwszy el. str. przekazany jako drugi parametr

        return true;
    }




    /**
     *  Publiczna metoda wykonuj�ca szkieletyzacj� obrazu elementami 
     *  strukturalnymi przekazanymi jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @param elStr1   element strukturalny KA
     *  @param elStr2   element strukturalny KB
     *  @param elStr3   element strukturalny RA
     *  @param elStr4   element strukturalny RB
     *  @return   bitmapa wyj�ciowa
     */
    public static JBitmap skeleton(JBitmap bin) {

        // bitmapa wyj�ciowa i pomocnicza
        JBitmap bout = new JBitmap( JSkeleton.skeleton(bin.getObjectPoints(),bin.getImageRectangle()),bin.getWidth(), bin.getHeight());
        return bout;
    }

    public static Set<Point> skeleton(Set<Point> image, Rectangle imageRectangle) {
        return image;
    }

}