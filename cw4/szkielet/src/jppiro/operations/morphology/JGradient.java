/*
 *  JGradient.java
 *
 *  Zawiera klas� JGradient,
 *  wykonuj�c� gradient morfologiczny obrazu wybranym elementem strukturalnym.
 */


// definicja pakietu
package jppiro.operations.morphology;


import java.awt.Point;
import java.util.Set;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;




/**
 *  Publiczna klasa JGradient wykonuje gradient morfologiczny obrazu
 *  wybranym elementem strukturalnym.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JGradient extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JGradient() {

        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Gradient " );

        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class, JStrEl.class );
        setParamDesc( "Obraz podstawowy", "Element strukturalny" );
    }




    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo�anie funkcji gradient()
        return (Object) gradient( (JBitmap)param[0],
                                  (JStrEl)param[1] );
    }




    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {

        // pierwszy el. str. przekazany jako drugi parametr
        JStrEl elStr1 =(JStrEl) param[1];

        // je�eli nie przekazano el. str.
        if( elStr1 == null ) {

            JOptionPane.showMessageDialog( null,
                    "Nie przekazano pierwszego el. str.!",
                    "Niepoprawny parametr!",
                    JOptionPane.WARNING_MESSAGE);

            return false;
        }

        return true;
    }




    /**
     *  Publiczna metoda wykonuj�ca gradient morfologiczny obrazu
     *  elementem strukturalnym przekazanym jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @param elStr   element strukturalny
     *  @return   bitmapa wyj�ciowa
     */
    public static JBitmap gradient(JBitmap bin, JStrEl elStr) {

        // bitmapa wyj�ciowa
        Set<Point> finalSet=JGradient.gradient(bin.getObjectPoints(),elStr.getVectors());
        // dodanie do listy operacji na obrazie informacji o wykonanej operacji 
        
        JBitmap bout=new JBitmap(finalSet, bin.getWidth(), bin.getHeight());
        bout.setOperations( bin );
        bout.addOperation( " gradient(" + elStr.getName() + ")" );

        // zwr�cenie bitmapy po wykonaniu operacji
        return bout;
    }
    

  public static Set<Point> gradient(Set<Point> image, Set<Point> elStr) {
        return image;               
   }
}