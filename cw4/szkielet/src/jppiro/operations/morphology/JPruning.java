/*
 * JPruning.java
 *
 * Created on 9 listopad 2006, 09:45
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package jppiro.operations.morphology;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.Set;
import jppiro.algorithms.JBitmap;
import jppiro.operations.JOperation;

/**
 *
 * @author adam
 */
public class JPruning extends JOperation {
    
    /** Creates a new instance of JPruning */
    public JPruning() {
        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Pruning " );

        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class );
        setParamDesc(new String[] { "Obraz podstawowy"});
                               
    }




    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo�anie funkcji skeleton()
        return (Object) pruning( (JBitmap)param[0]);
    }




    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {

        // pierwszy el. str. przekazany jako drugi parametr

        return true;
    }




    /**
     *  Publiczna metoda wykonuj�ca szkieletyzacj� obrazu elementami 
     *  strukturalnymi przekazanymi jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @param elStr1   element strukturalny KA
     *  @param elStr2   element strukturalny KB
     *  @param elStr3   element strukturalny RA
     *  @param elStr4   element strukturalny RB
     *  @return   bitmapa wyj�ciowa
     */
    public static JBitmap pruning(JBitmap bin) {

        // bitmapa wyj�ciowa i pomocnicza
        JBitmap bout = new JBitmap( JPruning.pruning(bin.getObjectPoints(),bin.getImageRectangle()),
                bin.getWidth(), bin.getHeight());
        return bout;
    }

    public static Set<Point> pruning(Set<Point> image, Rectangle imageRectangle) {
        return image;
    }
}