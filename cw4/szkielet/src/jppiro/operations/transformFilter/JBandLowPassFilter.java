/*
 *  JBandLowPassFilter.java
 *
 *  Zawiera klas� JBandLowPassFilter,
 *  wykonuj�c� opaskowy filtr dolnoprzepustowy na obrazie w dziedzinie
 *  transformaty Fouriera.
 */


// definicja pakietu
package jppiro.operations.transformFilter;


// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;




/**
 *  Publiczna klasa JBandLowPassFilter wykonuje opaskowy filtr dolnoprzepustowy
 *  na obrazie w dziedzinie transformaty Fouriera.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JBandLowPassFilter extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JBandLowPassFilter() {

        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Band LowPass Filter " );

        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JTransform.class, Integer.class, Integer.class );
        setParamDesc( "Transformata Fouriera", "Promie� wewn�trzny", "Promie� zewn�trzny" );
    }




    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo�anie funkcji bandLowPassFilter()
        return (Object) bandLowPassFilter( (JTransform)param[0],
                                           (Integer)param[1],
                                           (Integer)param[2] );
    }




    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {

        // transformata przekazana jako pierwszy parametr
        JTransform transform =(JTransform) param[0];

        // promie� wewn�trzny przekazany jako drugi parametr
        int inRad =(Integer) param[1];

        // je�eli warto�� wewn�trznego promienia jest < 0
        if( inRad <= 0 ) {

            JOptionPane.showMessageDialog( null,
                    "Warto�� wewn�trznego promienia musi by� > 0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        // je�eli warto�� wewn�trznego promienia jest > od po�owy rozmiaru obrazu
        if( inRad > transform.getWidth()/2 ) {

            JOptionPane.showMessageDialog( null,
                    "Warto�� wewn�trznego promienia musi by� <= " + transform.getWidth()/2,
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        // promie� zewn�trzny przekazany jako trzeci parametr
        int outRad =(Integer) param[2];

        // je�eli warto�� zewn�trznego promienia jest < 0
        if( outRad <= 0 ) {

            JOptionPane.showMessageDialog( null,
                    "Warto�� zewn�trznego promienia musi by� > 0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        // je�eli warto�� zewn�trznego promienia jest > od po�owy rozmiaru obrazu
        if( outRad > transform.getWidth()/2 ) {

            JOptionPane.showMessageDialog( null,
                    "Warto�� zewn�trznego promienia musi by� <= " + transform.getWidth()/2,
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        // je�eli warto�� wewn�trznego promienia jest > zewn�trznego promienia
        if( inRad >= outRad ) {

            JOptionPane.showMessageDialog( null,
                    "Warto�� zewn�trznego promienia musi by� < wewn�trznego promienia",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        return true;
    }




    /**
     *  Publiczna metoda wykonuj�c� opaskowy filtr dolnoprzepustowy na obrazie w
     *  dziedzinie transformaty Fouriera przekazanym jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param transform   dyskretna transformata Fouriera
     *  @param inRad   promie� wewn�trznego okr�gu
     *  @param outRad   promie� zewn�trznego okr�gu
     *  @return   transformata wyj�ciowa
     */ 
    public static JTransform bandLowPassFilter(JTransform transform, int inRad, int outRad) {

        // transformata wyj�ciowa        
        JTransform out = new JTransform( transform );

        // �rednica wewn�trznego i zewn�trznego okr�gu
        double inS = inRad * inRad;
        double outS = outRad * outRad;

        // wsp�rz�dne
        int x2, y2;

        // dla ka�dego piksela obrazu w dziedzinie transformatorowej
        for(int y=0; y<transform.getHeight(); y++) {

            for(int x=0; x<transform.getWidth(); x++) {

                // ustalenie po�o�enia piksela w przesuni�tym obrazie
                if( x >= transform.getWidth()/2 ) {

                    x2 = x - transform.getWidth();
                }
                else {

                    x2 = x;
                }

                if( y >= transform.getHeight()/2 ) {

                    y2 = y - transform.getHeight();
                }
                else {

                    y2 = y;
                }

                // odleg�o�� punktu od �rodka
                double d = x2*x2 + y2*y2;

                // je�eli punkt le�y pomi�dzy okr�gami
                if( d > inS  &&  d < outS ) {

                    // zerowanie warto�ci
                    out.setPixel( x, y, new JComplex( 0.0, 0.0 ) );
                }
            }
        }

        return out;
    }


}