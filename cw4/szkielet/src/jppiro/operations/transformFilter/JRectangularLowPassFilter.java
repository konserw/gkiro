/*
 *  JRectangularLowPassFilter.java
 *
 *  Zawiera klas� JRectangularLowPassFilter,
 *  wykonuj�c� prostok�tny filtr dolnoprzepustowy na obrazie w dziedzinie
 *  transformaty Fouriera.
 */


// definicja pakietu
package jppiro.operations.transformFilter;


// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;




/**
 *  Publiczna klasa JRectangularLowPassFilter wykonuje prostok�tny filtr
 *  dolnoprzepustowy na obrazie w dziedzinie transformaty Fouriera.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JRectangularLowPassFilter extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JRectangularLowPassFilter() {

        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Rectangular LowPass Filter " );

        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JTransform.class, Integer.class );
        setParamDesc( "Transformata Fouriera", "Rozmiar (po�owa boku prostok�ta)" );
    }




    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo�anie funkcji rectangularLowPassFilter()
        return (Object) rectangularLowPassFilter( (JTransform)param[0],
                                                  (Integer)param[1]);
    }




    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {

        // transformata przekazana jako pierwszy parametr
        JTransform transform =(JTransform) param[0];

        // rozmiar przekazany jako drugi parametr
        int size =(Integer) param[1];

        // je�eli warto�� rozmiaru jest < 0
        if( size <= 0 ) {

            JOptionPane.showMessageDialog( null,
                    "Warto�� rozmiaru musi by� > 0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        // je�eli warto�� rozmiaru jest > od po�owy rozmiaru obrazu
        if( size > transform.getWidth()/2 ) {

            JOptionPane.showMessageDialog( null,
                    "Warto�� rozmiaru musi by� <= " + transform.getWidth()/2,
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        return true;
    }




    /**
     *  Publiczna metoda wykonuj�c� prostok�tny filtr dolnoprzepustowy na obrazie w
     *  dziedzinie transformaty Fouriera przekazanym jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param transform   dyskretna transformata Fouriera
     *  @param size   rozmiar po�owy boku prostok�ta
     *  @return   transformata wyj�ciowa
     */ 
    public static JTransform rectangularLowPassFilter(JTransform transform, int size) {

     
        // pusta transformata wyj�ciowa
        JTransform out = new JTransform( transform.getWidth(),transform.getHeight() );

     
        return out;
    }


}