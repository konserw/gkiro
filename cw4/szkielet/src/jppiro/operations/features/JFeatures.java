/*
 * JFeatures.java
 *
 * Created on 9 listopad 2006, 17:29
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package jppiro.operations.features;

import java.awt.Point;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import jppiro.algorithms.JBitmap;
import jppiro.operations.JOperation;
import jppiro.operations.morphology.JInternalGradient;

/**
 *
 * @author adam
 */
public class JFeatures extends JOperation {
    
    /** Creates a new instance of JFeatures */
    public JFeatures() {
        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Features " );
        
        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class );
        setParamDesc( "Obraz podstawowy" );
        
        // ustawienie opisu wyniku funkcji
        setResultDesc( "Wsp�czynniki kszta�tu" );
    }
    
    
    
    
    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {
        
        // wywo�anie funkcji circular1()
        return (Object) features( (JBitmap)param[0] );
    }
    
    
    
    
    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {
        
        // obraz podstawowy przekazany jako pierwszy parametr
        JBitmap bin =(JBitmap) param[0];
        
        return true;
    }
    
    
    
    
    /**
     *  Publiczna metoda obliczaj�ca pierwszy wsp�czynnik cyrkularno�ci
     *  obrazu przekazanego jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @return   warto�� pierwszego wsp�czynnika cyrkularno�ci
     */
    public static String[] features(JBitmap bin) {
        
        // wyznaczenie wsp�rz�dnych punkt�w w obrazie
        Set<Point> image=bin.getObjectPoints();
        return new String[] {"Moment zwyk�y (2,2): "+mi(image,2,2),
        "Moment centralny CMi(2,2): "+miCentered(image,2,2),
        "Znormalizowany moment centralany (2,2): "+miNormalized(image,2,2),
        "Moment zwyk�y(3,3): "+mi(image,3,3),
        "Moment centralnyi(3,3): "+miCentered(image,3,3),
        "Znormalizowany moment centralany(3,3): "+miNormalized(image,3,3),
        "Niezmiennik 1: "+fi1(image),
        "Niezmiennik 2: "+fi2(image),
        "Niezmiennik 3: "+fi3(image),
        "Stosunek wsp�czynnik�w cyrkularno�ci RC1/RC2: "+RC1RC2(image),
        "Wsp�czynnik Haralicka : "+haralick(image)};
    }
    
    public static  double mi(Set<Point> image, int p, int q) {
        return 0;
    }
    
    public static double miCentered(Set<Point> image,int p, int q) {
        return 0;
    }
    
    public static double miNormalized(Set<Point> image,int p, int q) {
        return 0;
    }
    
    public static double RC1RC2(Set<Point> image) {
        return 0;
    }
    
    public static double haralick(Set<Point> image) {
        return 0;
    }
    
    public static double fi1(Set<Point> image) {
        return 0;
    }
    
    public static double fi2(Set<Point> image) {
        return 0;
    }
    
    public static double fi3(Set<Point> image) {
        return 0;
    }
    
}
