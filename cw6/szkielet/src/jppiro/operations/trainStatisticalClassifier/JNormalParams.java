/*
 * JNormalParams.java
 *
 * Created on 25 listopad 2006, 02:37
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package jppiro.operations.trainStatisticalClassifier;

import jppiro.algorithms.JMatrix;

/**
 * Obiekty tej klasy zawieraj� informacje wyznaczaone w fazie uczenia klasyfikatora parametrycznego z rozk�adem normalnym dla zadanej klasy
 * @author adam
 */
public class JNormalParams {
    
    /** Macierz kolumnowa warto�ci �rednich */
    public JMatrix M;
    
    /** Macierz kowariancji */
    public JMatrix C;
    
    /** Prawdopodobie�stwo wyst�pienia zadanej klasy*/
    public double pw;
    
    /** Creates a new instance of JNormalParams */
    public JNormalParams(JMatrix M, JMatrix C, double pw) {
        this.M=M;
        this.C=C;
        this.pw=pw;
    }
    
}
