/*
 * JHierarchicalClustering.java
 *
 * Created on 3 stycze� 2007, 23:40
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package jppiro.algorithms.clustering;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import jppiro.algorithms.JVector;
import jppiro.operations.JOperation;

/**
 *
 * @author adam
 */
public class JHierarchicalClustering extends JClustering  {
    
    private JGroupDistance groupDistance;
    
    /** Creates a new instance of JHierarchicalClustering */
    public JHierarchicalClustering(JGroupDistance groupDistance) {
        this.groupDistance=groupDistance;
    }
    
    public void clustering(JClustersSet clustersSet) {
        
        clustersSet.rearanageClusters(this.clustering(clustersSet.getVectors(), 
                clustersSet.getNumberOfClusters()));
    }
    
    private List<List<JVector>> clustering(JVector[] vectors, int numberOfClusters) {
        
        List<List<JVector>> groups=new LinkedList<List<JVector>>();
        for(int i=0;i<vectors.length;i++) {
            List<JVector> group=new LinkedList<JVector>();
            group.add(vectors[i]);
            groups.add(group);
        }
        
        int initialNumberOfGroups=groups.size();
                
        while(groups.size()>numberOfClusters) {
            System.out.println(groups.size());
            int g1=0;
            int g2=1;
            double dmin=Double.MAX_VALUE;
            for(int i=0;i<groups.size();i++) {
                for(int j=i+1;j<groups.size();j++) {
                    double d=this.groupDistance.distance(groups.get(i),groups.get(j));
                    /*
                     Tutaj nale�y uzupe�ni� !!! 
                     */
                           
                }
            }
            List<JVector> joinedGroup=groups.remove(g2);
            groups.get(g1).addAll(joinedGroup);
            if(this.operation!=null) {
                    operation.setProgress((float)(initialNumberOfGroups-groups.size())/
                            (initialNumberOfGroups-numberOfClusters));
                }

        }
        return groups;
        
    }
}
