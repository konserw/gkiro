/*
 * JGroupDistance.java
 *
 * Created on 5 stycze� 2007, 00:05
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package jppiro.algorithms.clustering;

import java.util.List;
import jppiro.algorithms.JVector;

/**
 *
 * @author adam
 */
public interface JGroupDistance {
 
    public double distance(List<JVector> group1, List<JVector> group2);
    
    
    public static JGroupDistance DMIN= new JGroupDistance() {
        public double distance(List<JVector> group1, List<JVector> group2) {
            double dmin=Double.MAX_VALUE;
            for(int i=0;i<group1.size();i++)
                for(int j=0;j<group2.size();j++) {
                    double d=group1.get(i).distance(group2.get(j));
                    if(dmin>d)
                        dmin=d;
                }
                    
            return dmin;
        }
        
        public String toString() {
            return "DMIN";
        }
        
    };
    
    public static JGroupDistance DMAX= new JGroupDistance() {
        public double distance(List<JVector> group1, List<JVector> group2) {
            double dmax=-Double.MAX_VALUE;
            for(int i=0;i<group1.size();i++)
                for(int j=0;j<group2.size();j++) {
                    double d=group1.get(i).distance(group2.get(j));
                    if(dmax<d)
                        dmax=d;
                }
                    
            return dmax;
        }
        public String toString() {
            return "DMAX";
        }
    };
    
    
    public static JGroupDistance DAVG= new JGroupDistance() {
        public double distance(List<JVector> group1, List<JVector> group2) {
            double dsum=0;
            for(int i=0;i<group1.size();i++)
                for(int j=0;j<group2.size();j++) {
                    dsum+=group1.get(i).distance(group2.get(j));
                }
                    
            return dsum/group1.size()*group2.size();
        }
        public String toString() {
            return "DAVG";
        }
    };
    
    public static JGroupDistance DMEAN= new JGroupDistance() {
                public double distance(List<JVector> group1, List<JVector> group2) {
            JVector dmean=new JVector(group1.get(0).getSize());
            for(int i=0;i<group1.size();i++)
                dmean.add(group1.get(i));
            for(int j=0;j<group2.size();j++) 
                dmean.sub(group2.get(j));    
            
            
                    
            return dmean.distance(new JVector(dmean.getSize()));
        }
        public String toString() {
            return "DMEAN";
        }
    };
    
    public JGroupDistance[] GROUP_DISTANCES = {DMIN, DMAX, DAVG, DMEAN} ;

}
