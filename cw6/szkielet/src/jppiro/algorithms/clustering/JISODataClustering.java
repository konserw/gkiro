/*
 * JISODataClustering.java
 *
 * Created on 3 stycze� 2007, 19:01
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package jppiro.algorithms.clustering;

import jppiro.algorithms.JMatrix;
import jppiro.algorithms.JVector;

/**
 *
 * @author adam
 */
public class JISODataClustering extends JClustering {
    
    private int numberOfIterations;
    private double epsilon;
    
    /** Creates a new instance of JISODataClustering */
    public JISODataClustering(int numberOfIterations, double epsilon) {
        this.numberOfIterations=numberOfIterations;
        this.epsilon=epsilon;
    }
    
    public void clustering(JClustersSet clustersSet) {
        
        for(int i=0;i<this.numberOfIterations;i++) {
            JMatrix oldMU=new JMatrix(clustersSet.getMU());
            JVector[] clustersCenters=clustersSet.getClustersCenters(1);


            for(int j=0;j<clustersSet.getNumberOfSamples();j++) {
                int nearestCluster=0;
                double dmin=Double.MAX_VALUE;
                for(int k=0;k<clustersCenters.length;k++) {
                    double d=clustersCenters[k].distance(clustersSet.getVector(j));
                    if(d<dmin) {
                        nearestCluster=k;
                        dmin=d;
                    }
                }
                
                clustersSet.setCluster(j,nearestCluster);
            }
            
            oldMU=oldMU.sub(clustersSet.getMU());
            double norm=oldMU.calculate3rdNorm();
            if(this.operation!=null) {
                operation.setProgress((float)i/numberOfIterations);
            }
            if(norm<epsilon)
                break;
           
        }
        
    }
    
    
}
