 /*
 *  JDynamicThresholding.java
 *
 *  Zawiera klas� JDynamicThresholding,
 *  wykonuj�c� operacj� dynamicznego progowania obrazu.
 */


// definicja pakietu
package jppiro.operations.thresholding;


// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;




/**
 *  Publiczna klasa JDynamicThresholding wykonuje operacj� dynamicznego
 *  progowania obrazu.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JDynamicThresholding extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JDynamicThresholding() {

        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Dynamic Thresholding " );

        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class, Integer.class, Integer.class );
        setParamDesc( "Obraz podstawowy", "Rozmiar maski otoczenia (nieparzysty >=3)",
                      "Sta�a do odj�cia" );
    }




    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo�anie funkcji dynamicThresholding()
        return (Object) dynamicThresholding( (JBitmap)param[0], 
                                             (Integer)param[1], 
                                             (Integer)param[2] );
    }




    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {

        // rozmiar maski otoczenia przekazany jako drugi parametr
        int maskSize =(Integer) param[1];

        // je�eli rozmiar maski jest < 3
        if( maskSize < 3 ) {

            JOptionPane.showMessageDialog( null,
                    "Rozmiar maski musi by� >= 3",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        // je�eli rozmiar maski > 49
        if( maskSize > 49 ) {

            JOptionPane.showMessageDialog( null,
                    "Rozmiar maski musi by� <= 49",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        // je�eli rozmiar maski jest parzysty
        if( maskSize % 2 == 0 ) {

            JOptionPane.showMessageDialog( null,
                    "Warto�� rozmiaru maski musi byc nieparzysta",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        // warto�� sta�a do odj�cia przekazana jako trzeci parametr
        int subConst=(Integer) param[2];

        // je�eli sta�a do odj�cia < 0
        if( subConst < 0 ) {

            JOptionPane.showMessageDialog( null,
                    "Warto�� sta�ej do odj�cia musi by� >= 0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        // je�eli sta�a do odj�cia > 50
        if( subConst > 50 ) {

            JOptionPane.showMessageDialog( null,
                    "Warto�� sta�ej do odj�cia musi by� <= 50",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        return true;
    }




    /**
     *  Publiczna metoda wykonuj�ca operacj� dynamicznego progowania obrazu
     *  przekazanego jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @param maskSize   rozmiar maski otoczenia (liczba nieparzysta >=3)
     *  @param subConst   sta�a do odjecia
     *  @return   bitmapa wyj�ciowa
     */ 
    public static JBitmap dynamicThresholding(JBitmap bin, int maskSize, int subConst) {
        // bitmapa wyj�ciowa
        JBitmap bout = new JBitmap( bin.getWidth(), bin.getHeight() );

        // tablice pikseli bitmapy wej�ciowej, wyj�ciowej i prog�w lokalnych
        int binBits[] = bin.getPixels();
        int boutBits[] = bout.getPixels();
        int thresholdMap[] = new int[binBits.length];

        // wyznaczenie tablicy progow - tresholdMap[]
        int halfMask = maskSize >> 1;
        int sum, threshold;

        // dla ka?dego piksela bitmapy wej�ciowej
        for(int y=halfMask; y<bin.getHeight()-halfMask; y++) {

            for(int x=halfMask; x<bin.getWidth()-halfMask; x++) {

                sum = 0;
                // dla wszystkich pikseli z otoczenia rozpatrywanego piksela
                for(int j=-halfMask; j<=halfMask; j++) {

                    for(int i=-halfMask; i<=halfMask; i++) {

                        // je?eli piksel nie jest rozpatrywanym pikselem
                        if( i!=0  ||  j!=0 ) {

                            // dodanie warto�ci piksela do sumy
                            sum += bin.getPixel( x+i, y+j );
                        }
                    }
                }

                // obliczenie warto�ci progu dla rozpatrywanego piksela
                threshold = 0; //uzupe�ni�!!!!!
                thresholdMap[y*bin.getWidth() + x] = threshold;
            }

            // ustawienie stopnia zaawansowania oblicze�
            setProgress( (y*1.0)/bin.getHeight() );
        }

        // dla ka?dego piksela bitmapy
        for(int i=0; i<binBits.length; i++) {

            // je?eli warto�� piksela jest mniejsza lub r�wna progowi
            if( binBits[i] <= thresholdMap[i] ) {

                // piksel jest ustawiany na kolor czarny
                boutBits[i] = 0;
            }

            // je?eli warto�� piksela jest wi�ksza od progu
            if( binBits[i] > thresholdMap[i] ) {

                // piksel jest ustawiany na kolor bia?y
                boutBits[i] = 255;
            }
        }

        // dodanie do listy operacji na obrazie informacji o wykonanej operacji 
        bout.setOperations( bin );
        bout.addOperation( " dynamicThresholding(" + maskSize + ", " + subConst + ") " );

        // zwr�cenie bitmapy po wykonaniu operacji
        return bout;

    }


}