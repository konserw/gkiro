/*
 *  JCircularLowPassFilter.java
 *
 *  Zawiera klas� JCircularLowPassFilter,
 *  wykonuj�c� ko�owy filtr dolnoprzepustowy na obrazie w dziedzinie
 *  transformaty Fouriera.
 */


// definicja pakietu
package jppiro.operations.transformFilter;


// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;




/**
 *  Publiczna klasa JCircularLowPassFilter wykonuje ko�owy filtr dolnoprzepustowy
 *  na obrazie w dziedzinie transformaty Fouriera.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JCircularLowPassFilter extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JCircularLowPassFilter() {

        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Circular LowPass Filter " );

        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JTransform.class, Integer.class );
        setParamDesc( "Transformata Fouriera", "Promie�" );
    }




    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo�anie funkcji circularLowPassFilter()
        return (Object) circularLowPassFilter( (JTransform)param[0],
                                                (Integer)param[1]);
    }




    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {

        // transformata przekazana jako pierwszy parametr
        JTransform transform =(JTransform) param[0];

        // promie� przekazany jako drugi parametr
        int radius =(Integer) param[1];

        // je�eli warto�� promienia jest < 0
        if( radius <= 0 ) {

            JOptionPane.showMessageDialog( null,
                    "Warto�� promienia musi by� > 0",
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        // je�eli warto�� promienia jest > od po�owy rozmiaru obrazu
        if( radius > transform.getWidth()/2 ) {

            JOptionPane.showMessageDialog( null,
                    "Warto�� promienia musi by� <= " + transform.getWidth()/2,
                    "Niepoprawny parametr!", 
                    JOptionPane.WARNING_MESSAGE );

            return false;
        }

        return true;
    }




    /**
     *  Publiczna metoda wykonuj�c� ko�owy filtr dolnoprzepustowy na obrazie w
     *  dziedzinie transformaty Fouriera przekazanym jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param transform   dyskretna transformata Fouriera
     *  @param radius   promie� okr�gu
     *  @return   transformata wyj�ciowa
     */ 
    public static JTransform circularLowPassFilter(JTransform transform, int radius) {

        // transformata wyj�ciowa
        JTransform out = new JTransform( transform );


        return out;
    }


}