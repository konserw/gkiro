/*
 *  JBandHighPassFilter.java
 *
 *  Zawiera klas� JBandHighPassFilter,
 *  wykonuj�c� opaskowy filtr g�rnoprzepustowy na obrazie w dziedzinie
 *  transformaty Fouriera.
 */


// definicja pakietu
package jppiro.operations.transformFilter;


// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;




/**
 *  Publiczna klasa JBandHighPassFilter wykonuje opaskowy filtr g�rnoprzepustowy
 *  na obrazie w dziedzinie transformaty Fouriera.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JCustomFilter extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JCustomFilter() {

        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Custom Filter " );

        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JTransform.class, Integer.class, Integer.class );
        setParamDesc( "Transformata Fouriera", "P1", "P2" );
    }




    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo�anie funkcji bandHighPassFilter()
        return (Object) customFilter( (JTransform)param[0],
                                            (Integer)param[1],
                                            (Integer)param[2] );
    }




    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {

        // transformata przekazana jako pierwszy parametr
        JTransform transform =(JTransform) param[0];

        // promie� wewn�trzny przekazany jako drugi parametr
        return true;
    }




    /**
     *  Publiczna metoda wykonuj�c� opaskowy filtr g�rnoprzepustowy na obrazie w
     *  dziedzinie transformaty Fouriera przekazanym jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param transform   dyskretna transformata Fouriera
     *  @param inRad   promie� wewn�trznego okr�gu
     *  @param outRad   promie� zewn�trznego okr�gu
     *  @return   transformata wyj�ciowa
     */ 
    public static JTransform customFilter(JTransform transform, int p1, int p2) {

        // transformata wyj�ciowa        
        JTransform out = new JTransform( transform );

        return out;
    }


}