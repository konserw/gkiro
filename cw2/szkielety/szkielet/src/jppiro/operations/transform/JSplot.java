/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jppiro.operations.transformFilter;


// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;




/**
 *  Publiczna klasa JBandHighPassFilter wykonuje opaskowy filtr g�rnoprzepustowy
 *  na obrazie w dziedzinie transformaty Fouriera.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JSplot extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JSplot() {

        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Splot Filter " );

        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JTransform.class, JTransform.class);
        setParamDesc( "Transformata Fouriera", "Transformata Fouriera");
    }




    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo�anie funkcji bandHighPassFilter()
        return (Object) splotFilter( (JTransform)param[0],
                                            (JTransform)param[1]);
                                            
    }




    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {

        // transformata przekazana jako pierwszy parametr
        JTransform transform =(JTransform) param[0];

        // promie� wewn�trzny przekazany jako drugi parametr
        return true;
    }




    /**
     *  Publiczna metoda wykonuj�c� opaskowy filtr g�rnoprzepustowy na obrazie w
     *  dziedzinie transformaty Fouriera przekazanym jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param transform   dyskretna transformata Fouriera
     *  @param inRad   promie� wewn�trznego okr�gu
     *  @param outRad   promie� zewn�trznego okr�gu
     *  @return   transformata wyj�ciowa
     */ 
    public static JTransform splotFilter(JTransform t1,JTransform t2) {

        // transformata wyj�ciowa        
        JTransform out = new JTransform( t1 );
        
        for(int k=0;k<out.getWidth();k++)
            for(int l=0;l<out.getHeight();l++) {
                JComplex val=JComplex.mul(t1.getPixel(k, l), t2.getPixel(k, l));
                out.setPixel(k, l, val);
            }

        return out;
    }


}