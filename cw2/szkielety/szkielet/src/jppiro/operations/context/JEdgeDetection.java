/*
 *  JEdgeDetection.java
 *
 *  Zawiera klas� JEdgeDetection,
 *  wykonuj�c� operacj� wykrywania kraw�dzi.
 */


// definicja pakietu
package jppiro.operations.context;


// importowane pakiety
import jppiro.*;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;




/**
 *  Publiczna klasa JEdgeDetection wykonuje operacje wykrywania kraw�dzi.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JEdgeDetection extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JEdgeDetection() {

        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Edge Detection " );

        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class, JMask.class, JMask.class, Integer.class );
        setParamDesc( "Obraz podstawowy", "Maska pozioma", "Maska pionowa", "Wsp�czynnik 'k'");
    }




    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo�anie funkcji edgeDetection()
        return (Object) edgeDetection( (JBitmap)param[0],
                                       (JMask)param[1],
                                       (JMask)param[2],
                                       (Integer)param[3] );
    }




    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {

        // pierwsza maska przekazana jako drugi parametr
        JMask mask1 =(JMask) param[1];

        // je�eli nie przekazano maski
        if( mask1 == null ) {

            JOptionPane.showMessageDialog( null,
                    "Nie przekazano pierwszej maski!",
                    "Niepoprawny parametr!",
                    JOptionPane.WARNING_MESSAGE);

            return false;
        }

        // druga maska przekazana jako trzeci parametr
        JMask mask2 =(JMask) param[2];

        // je�eli nie przekazano maski
        if( mask2 == null ) {

            JOptionPane.showMessageDialog( null,
                    "Nie przekazano drugiej maski!",
                    "Niepoprawny parametr!",
                    JOptionPane.WARNING_MESSAGE);

            return false;
        }
        // wsp�czynik 'k' przekazany jako czwarty parametr
        int k =(Integer) param[3];

        // je�eli wsp�czynnik k <= 0
        if( k <= 0 ) {

            JOptionPane.showMessageDialog( null,
                    "Parametr 'k' musi by� > 0!",
                    "Niepoprawny parametr!",
                    JOptionPane.WARNING_MESSAGE);

            return false;
        }


        return true;
    }




    /**
     *  Publiczna metoda wykonuj�ca operacj� wykrywania kraw�dzi 
     *  maskami przekazanymi jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @param horMask   maska pozioma
     *  @param verMask   maska pionowa
     *  @param k   wsp�czynnik 'k'
     *  @return   bitmapa wyj�ciowa
     */
    public static JBitmap edgeDetection(JBitmap bin, JMask horMask, JMask verMask, int k) {

        // bitmapy wyj�ciowa, pozioma i pionowa
        JBitmap bout = new JBitmap( bin );

        return bout;
    }


}