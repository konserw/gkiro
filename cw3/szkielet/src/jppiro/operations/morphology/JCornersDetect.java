/*
 *  JCornersDetect.java
 *
 *  Zawiera klas� JCornersDetect,
 *  wykonuj�c� detekcje naro�nik�w w obrazie wybranymi elementami strukturalnymi.
 */


// definicja pakietu
package jppiro.operations.morphology;


import java.awt.Point;
import java.awt.Rectangle;
import java.util.HashSet;
import java.util.Set;
import jppiro.algorithms.*;
import jppiro.operations.*;

/**
 *  Publiczna klasa JCornersDetect wykonuje detekcje naro�nik�w w obrazie
 *  wybranymi elementami strukturalnymi.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JCornersDetect extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JCornersDetect() {
        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Corners " );
        
        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class);
        setParamDesc( "Obraz podstawowy" );
    }
    
    public Object run(Object ... param) {
        
        // wywo�anie funkcji skeleton()
        return (Object) corners( (JBitmap)param[0]);
    }
    
    public static JBitmap corners(JBitmap bin) {
        
        Set<Point> corners=JCornersDetect.corners(bin.getObjectPoints(), bin.getImageRectangle());
        return new JBitmap(corners, bin.getWidth(), bin.getHeight());
    }
    
    public static Set<Point> corners(Set<Point> image, Rectangle imageRectangle) {
        Set<Point> strA2=new HashSet<Point>();
        strA2.add(new Point(-1,0));
        strA2.add(new Point(0,0));
        strA2.add(new Point(0,1));
        
        Set<Point> strB2=new HashSet<Point>();
        strB2.add(new Point(0,-1));
        strB2.add(new Point(1,-1));
        strB2.add(new Point(1,0));
        
        Set<Point> corners=new HashSet<Point>();
        for(int i=0;i<4;i++) {
            Set<Point> corner=JHitMiss.hitMiss(image,imageRectangle,JStrEl.rotate(strA2,i*Math.PI/2),
                    JStrEl.rotate(strB2,i*Math.PI/2));
            corners.addAll(corner);
        }
        
        return corners;
    }
    public boolean paramVerification(Object ... param) {
        return true;

    }
}