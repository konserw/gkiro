/*
 *  JHitMiss.java
 *
 *  Zawiera klas� JHitMiss,
 *  wykonuj�c� operacj� HitMiss na obrazie.
 */


// definicja pakietu
package jppiro.operations.morphology;


import java.awt.Point;
import java.awt.Rectangle;
import java.util.Set;
import jppiro.algorithms.*;
import jppiro.operations.*;
import javax.swing.JOptionPane;




/**
 *  Publiczna klasa JHitMiss wykonuje operacje HitMiss na obrazie.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JHitMiss extends JOperation
{


    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JHitMiss() {

        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "HitMiss " );

        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class, JStrEl.class, JStrEl.class );
        setParamDesc( "Obraz podstawowy", "Element strukturalny 1", 
                      "Element strukturalny 2" );
    }




    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo�anie funkcji hitMiss()
        return (Object) hitMiss( (JBitmap)param[0],
                                 (JStrEl)param[1],
                                 (JStrEl)param[2] );
    }




    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {

        // pierwszy el. str. przekazany jako drugi parametr
        JStrEl elStr1 =(JStrEl) param[1];

        // je�eli nie przekazano el. str.
        if( elStr1 == null ) {

            JOptionPane.showMessageDialog( null,
                    "Nie przekazano pierwszego el. str.!",
                    "Niepoprawny parametr!",
                    JOptionPane.WARNING_MESSAGE);

            return false;
        }

        // drugi el. str. przekazany jako trzeci parametr
        JStrEl elStr2 =(JStrEl) param[2];

        // je�eli nie przekazano el. str.
        if( elStr2 == null ) {

            JOptionPane.showMessageDialog( null,
                    "Nie przekazano drugiego el. str.!",
                    "Niepoprawny parametr!",
                    JOptionPane.WARNING_MESSAGE);

            return false;
        }

        return true;
    }




    /**
     *  Publiczna metoda wykonuj�ca erozj� obrazu elementem strukturalnym
     *  przekazanym jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @param elStr1   pierwszy element strukturalny
     *  @param elStr2   drugi element strukturalny
     *  @return   bitmapa wyj�ciowa
     */
    public static JBitmap hitMiss(JBitmap bin, JStrEl elStr1, JStrEl elStr2) {

        // bitmapa wyj�ciowa
        // zwr�cenie bitmapy po wykonaniu operacji
        Set<Point> out=JHitMiss.hitMiss(bin.getObjectPoints(),bin.getImageRectangle(),
                elStr1.getVectors(), elStr2.getVectors());
        
        
        
        
        return new JBitmap(out,bin.getWidth(),bin.getHeight());
    }
    
    public static Set<Point> hitMiss(Set<Point> image,Rectangle imageRectangle, Set<Point> strHit, Set<Point> strMiss) {
        return image;
    }


}