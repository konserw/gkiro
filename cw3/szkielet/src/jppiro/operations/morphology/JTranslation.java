/*
 * JTranslation.java
 *
 * Created on 9 listopad 2006, 14:57
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package jppiro.operations.morphology;

import java.awt.Point;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import jppiro.algorithms.JBitmap;
import jppiro.operations.JOperation;

/**
 *
 * @author adam
 */
public class JTranslation extends JOperation {
    
    /** Creates a new instance of JTranslation */
    public JTranslation() {
          // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Translation " );

        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class, Integer.class, Integer.class );
        setParamDesc( "Obraz podstawowy", "x", "y" );
    }

    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {

        // wywo�anie funkcji dilation()
        return (Object) translation( (JBitmap)param[0],
                                 new Point(((Integer)param[1]).intValue(), 
                                ((Integer)param[2]).intValue()));
    }




    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {

        // pierwszy el. str. przekazany jako drugi parametr
        Integer x =(Integer) param[1];
        Integer y =(Integer) param[2];
        // je�eli nie przekazano el. str.

        return true;
    }




    /**
     *  Publiczna metoda wykonuj�ca dylatacj� obrazu elementem strukturalnym
     *  przekazanym jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @param elStr   element strukturalny
     *  @return   bitmapa wyj�ciowa
     */
    public static JBitmap translation(JBitmap bin, Point vector) {
        
        Set<Point> finalSet=JTranslation.translation(bin.getObjectPoints(),vector);
        JBitmap bout=new JBitmap(finalSet, bin.getWidth(), bin.getHeight());
        return bout;
    }




    /**
     *  Prywatna metoda wykonuj�ca translacj� obrazu o punkt 
     *  przekazany jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @param point   punkt
     *  @return   bitmapa wyj�ciowa
     */
 
      public static Set<Point> translation(Set<Point> image, Point vector) {
          return image;
      }
    
    

}