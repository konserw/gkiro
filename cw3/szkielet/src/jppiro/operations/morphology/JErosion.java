/*
 *  JErosion.java
 *
 *  Zawiera klas� JErosion,
 *  wykonuj�c� erozj� obrazu wybranym elementem strukturalnym.
 */


// definicja pakietu
package jppiro.operations.morphology;


import jppiro.algorithms.*;
import jppiro.operations.*;
import java.awt.Point;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.swing.JOptionPane;




/**
 *  Publiczna klasa JErosion wykonuje erozj� obrazu wybranym elementem strukturalnym.
 *
 *  @author Micha� W�do�owski
 *  @version 1.0    (09/2006)
 */
public class JErosion extends JOperation {
    
    
    /**
     *  Publiczny konstruktor bezparametrowy.
     */
    public JErosion() {
        
        // ustawienie nazwy funkcji (wy�wietlanej w menu programu)
        setName( "Erosion " );
        
        // ustawienie typ�w i opis�w parametr�w funkcji
        setParamTypes( JBitmap.class, JStrEl.class );
        setParamDesc( "Obraz podstawowy", "Element strukturalny" );
    }
    
    
    
    
    /**
     *  Publiczna metoda wywo�uj�ca funkcj� z jej parametrami.
     *
     *  @param param   parametry funkcji
     *  @return   wynik funkcji
     */
    public Object run(Object ... param) {
        
        // wywo�anie funkcji erosion()
        return (Object) erosion( (JBitmap)param[0],
                (JStrEl)param[1] );
    }
    
    
    
    
    /**
     *  Publiczna metoda weryfikuj�ca poprawno�� parametr�w.
     *
     *  @param param   parametry funkcji kt�re podlegaj� sprawdzeniu
     *  @return   true - je�eli parametry s� poprawne,
     *              false - je�li parametry nie s� poprawne
     */
    public boolean paramVerification(Object ... param) {
        
        // pierwszy el. str. przekazany jako drugi parametr
        JStrEl elStr1 =(JStrEl) param[1];
        
        // je�eli nie przekazano el. str.
        if( elStr1 == null ) {
            
            JOptionPane.showMessageDialog( null,
                    "Nie przekazano pierwszego el. str.!",
                    "Niepoprawny parametr!",
                    JOptionPane.WARNING_MESSAGE);
            
            return false;
        }
        
        return true;
    }
    
    
    
    
    /**
     *  Publiczna metoda wykonuj�ca erozj� obrazu elementem strukturalnym
     *  przekazanym jako parametr.
     *  Metoda statyczna - mo�e by� wywo�ana bez tworzenia obiektu.
     *
     *  @param bin   bitmapa wej�ciowa
     *  @param elStr   element strukturalny
     *  @return   bitmapa wyj�ciowa
     */
    public static JBitmap erosion(JBitmap bin, JStrEl elStr) {
        Set<Point> finalSet=JErosion.erosion(bin.getObjectPoints(),elStr.getVectors());
        
        JBitmap bout=new JBitmap(finalSet, bin.getWidth(), bin.getHeight());
        // wyznaczenie punkt�w elementu strukturalnego
        return bout;
        
    }
    
    
    
    public static Set erosion(Set<Point> image, Set<Point> strEl) {
        return image;
        
    }
    
    
    
}